var wishListLib = require('product-service-lib/src/wishListMvp/wishListMvpMgr');

exports.addWishList = function (req, res) {
  try {
    wishListLib.addWishList(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.removeWishList = function (req, res) {
  try {
    wishListLib.removeWishList(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.moveToAddToCart = function (req, res) {
  try {
    wishListLib.moveToAddToCart(req, res);
  } catch (error) {
    console.log(error);
  }
}


exports.moveToWishList = function (req, res) {
  try {
    wishListLib.moveToWishList(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.getWishList = function (req, res) {
  try {
    wishListLib.getWishList(req, res);
  } catch (error) {
    console.log(error);
  }
}