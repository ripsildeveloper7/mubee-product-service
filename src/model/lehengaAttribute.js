
var mongoose = require('mongoose');


const lehengaAttribute = new mongoose.Schema({
    lehengaFabric: String,
    blouseFabric: String,
    dupattaFabric: String,
    lehengaLiningFabric: String,
    lehengaClosure: String,
    hemline: String,
    blouseClosure: String,
    neck: String,
    sleeveLength: String,
    lehengaStitch: String,
    choliStitch: String,
    dupatta: String,
    topPattern: String,
    bottomPattern: String,
    dupattaPattern: String,
    dupattaBorder: String
});
module.exports = lehengaAttribute;
