var mongoose = require('mongoose');

const IncrementRateSchema = new mongoose.Schema({
    incRate: Number,
    addedDate: Date
});

const Rate = mongoose.model('incrementrate', IncrementRateSchema);

module.exports = Rate;