var mongoose = require('mongoose');

const PriceRateSchema = new mongoose.Schema({
    priceRate: Number,
    addedDate: Date
});

const Rate = mongoose.model('pricerate', PriceRateSchema);

module.exports = Rate;