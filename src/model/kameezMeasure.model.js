var mongoose = require('mongoose');
var value = require('./blouseMeasurementValue.model');
var style = require('./blouseMeasurementStyle.model');
const KameezMeasure = new mongoose.Schema({
    aroundBust: [value],
    aroundAboveWaist: [value],
    aroundHip: [value],
    kameezLength: [value],
    frontNeckStyle: [style],
    frontNeckDepth: [value],
    backNeckStyle: [style],
    backNeckDepth: [value],
    sleeveStyle: [style],
    sleeveLength: [value],
    aroundArm: [value],
    kameezClosingSide: [String],
    kameezClosingWith: [String]
});
module.exports = KameezMeasure;