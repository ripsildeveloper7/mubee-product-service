var mongoose = require('mongoose');
var ProductTagValue = require('./productTagValue.model');
const productTagModelSchema = new mongoose.Schema({
    tagName: String,
    position: String,
    tagValue: [ProductTagValue]
});

const productTag = mongoose.model('producttag', productTagModelSchema);
module.exports = productTag;
