var mongoose = require('mongoose');

const CategoryBannerSchema = new mongoose.Schema({
    title: String,
    imagePosition: Number,
    description: String,
    imageName: String,
    link: String,
    verticalPosition: String,
    horizontalPosition: String
});
const categoryBanner = mongoose.model('categorybanner', CategoryBannerSchema);
module.exports = categoryBanner;