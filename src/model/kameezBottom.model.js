var mongoose = require('mongoose');
var value = require('./blouseMeasurementValue.model');
var style = require('./blouseMeasurementStyle.model');
const KameezBottom = new mongoose.Schema({
    bottomStyle: [style],
    fitOption: [String],
    aroundWaist: [value],
    aroundThigh: [value],
    aroundKnee: [value],
    aroundCalf: [value],
    aroundBottom: [value],
    bottomLength: [value],
    waistClosingSide: [String],
    waistClosingWith: [String]
});

module.exports = KameezBottom;