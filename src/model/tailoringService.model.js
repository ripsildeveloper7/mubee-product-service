var mongoose = require('mongoose');
const tailoringServiceModelSchema = new mongoose.Schema({
    readyToWear: Boolean,
    semiStitched: Boolean,
    unStitched: Boolean
});
module.exports = tailoringServiceModelSchema;

