var mongoose = require('mongoose');
var semiStitchedValue = require('./semistitchValue.model');

const semiStitchedModelSchema = new mongoose.Schema({
    serviceName: String,
    serviceValue: [semiStitchedValue]
});

const semiStitched = mongoose.model('semistitch', semiStitchedModelSchema);
module.exports = semiStitched;

