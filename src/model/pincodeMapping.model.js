var mongoose = require('mongoose');
var PincodeDetails = require('./pincodeDetails.model')
var B2cMarketSchema = new mongoose.Schema({
    brandId: String,
    pincodeDetails: [{
        City: String,
        Pincode: String,
        HasCod: String,
        HasPrepaid: String,
        HasReverse: String,
        ShiprocketDeliveryZone: String,
        RoutingCode: String,
        State: String,
        TAT: String
    }]
    
}); 
 
const B2cMarket = mongoose.model('pincode', B2cMarketSchema);
module.exports = B2cMarket;