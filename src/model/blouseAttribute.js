
var mongoose = require('mongoose');


const blouseAttribute = new mongoose.Schema({
    neck: String,
    sleeveLength: String,
    occasion: String,
    acrossShoulderInches: String,
    bustInches: String,
    frontLengthInches: String
});
module.exports = blouseAttribute;
