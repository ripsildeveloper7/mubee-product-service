
var mongoose = require('mongoose');


const jewelleryAttribute = new mongoose.Schema({
    stoneType: String,
    plating: String,
    warranty: String,
    trends: String,
    numberOfComponents: String
});
module.exports = jewelleryAttribute;
