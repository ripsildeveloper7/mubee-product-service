var mongoose = require('mongoose');
const MeasurementStyleSchema = new mongoose.Schema({
    imageName: String,
    title: String
});
module.exports = MeasurementStyleSchema;