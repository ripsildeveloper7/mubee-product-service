var mongoose = require('mongoose');
var childData = require('./child.model');
var attributeData = require('./attribute.model');
var productImageName = require('./productImageName.model');

const ProductSchema = new mongoose.Schema({
  productName: String,
  manufactureInfo: String,
  brandName: String,
  hsnCode: String,
  gstIn: String,
  sku: String,
  styleCode: String,
  variation: String,
  productsType: String,
  variantType: String,
  variantId: String,
  variationType: String,
  PINTsku: String,
  INTsku: String,
  packOf: Number,
  size: String,
  sizeGuideName: String,
  sizeGuideId: {type:mongoose.Schema.Types.ObjectId, ref: 'SizeGuideSchema'},
  weight: String,
  fabric: String,
  washCare: String,
  sp: { type: Number, default: 0 },
  productDimension: String,
  costIncludes: String,
  catalogueName: String,
  packingDimension: String,
  discount: { type: Number, default: 0},
  mrp: { type: Number, default: 0},
  price: { type: Number, default: 0},
  closure: String,
  pattern: String,
  productDescription: String,
  attribute: [attributeData],
  ttsPortol: { type: Number, default: 0 },
  searchTerms1: String,
  searchTerms2: String,
  searchTerms3: String,
  searchTerms4: String,
  searchTerms5: String,
  variationId: String,
    sizeVariantId: String,
    variationTypeId: String,
    colorId: String,
  superCategoryId: {type: mongoose.Schema.Types.ObjectId, ref: 'SuperCategorySchema' },
  superCategoryName: String,
  mainCategoryId: {type:mongoose.Schema.Types.ObjectId, ref: 'SuperCategorySchema'},
  mainCategoryName: String,
  subCategoryId: {type: mongoose.Schema.Types.ObjectId, ref: 'SuperCategorySchema' },
  subCategoryName: String,
  color: String,
  tags: [{type: mongoose.Schema.Types.ObjectId, ref: 'productTagModelSchema' }],
  metaTitle: String,
  metaDescription: String,
  metaKeyword: String,
  dateAdded:  { type: Date, default: Date.now() },
  dateModified: { type: Date, default: Date.now() },
  brandId: {type: mongoose.Schema.Types.ObjectId, ref: 'BrandSchema' } ,
  sizeVariant: String,
  variantionType: String,
  publish: Boolean,
  sizeName: String,
  sku: String,
  quantity: Number,
  productImage: [productImageName],
  child: [childData],
  headChild: Boolean,
  tailoringService: String,
  warranty:String
});


const product = mongoose.model('product', ProductSchema);
module.exports = product;