var mongoose = require('mongoose');

const IDgeneratorSchema = new mongoose.Schema({
  INTsku: {type: String, default: "50100"},
  PINTsku : {type: String, default: "100"}
});

const IDgenerator = mongoose.model('IDgenerator', IDgeneratorSchema);
module.exports = IDgenerator;