var mongoose = require('mongoose');

const sizeModelSchema = new mongoose.Schema({
    productId: {type: mongoose.Schema.Types.ObjectId, ref: 'ProductSchema' } ,
    sizeName: String,
    sku: String,
    sizeQuantity: Number,
    sizePrice: Number, 
    pricePrefix: Boolean,
    subtractStack: Number
});

module.exports  = sizeModelSchema;
/* const size = mongoose.model('productvariant', sizeModelSchema);
module.exports = size; */