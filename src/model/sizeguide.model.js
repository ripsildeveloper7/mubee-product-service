var mongoose = require('mongoose');


const SizeGuideSchema = new mongoose.Schema({
    title: String,
    sizeGuideName: String,
    imageName: String,
    superCategoryId: String,
    mainCategoryId: String,
    subCategoryId: String,
    sizeChartCM: String,
    sizeChartInches: String,
    superCategoryName: String,
    mainCategoryName: String,
    subCategoryName: String,
    brandId: String,
    brandName: String
});


const SizeGuide = mongoose.model('sizeguide', SizeGuideSchema);
module.exports = SizeGuide;
