var mongoos = require('mongoose');
var kameez = require('./kameezMeasure.model');
var bottom = require('./kameezBottom.model');
const KameezMeasurementSchema = new mongoos.Schema({
    superCategoryId: String,
    measurementName: String,
    measurementType: [{ typeName: String, typeDescription: String}],
    creationDate: Date,
    modifiedDate: Date,
    price: Number,
    discount: Number,
    kameezMeasurement: [kameez],
    bottomMeasurement: [bottom]
});

var Kameez = mongoos.model('kameezmeasurement', KameezMeasurementSchema);

module.exports = Kameez;