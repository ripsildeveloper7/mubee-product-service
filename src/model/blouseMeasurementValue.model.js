var mongoose = require('mongoose');
const MeasurementValueSchema = new mongoose.Schema({
    min: Number,
    max: Number,
    imageName: String,
    description: String
});
module.exports = MeasurementValueSchema;