var mongoose = require('mongoose');


const bagAttribute = new mongoose.Schema({
    warranty: String,
    compartmentClosure: String,
    numberOfExternalPockets: String,
    numberOfMainCompartments: String,
    handless: String,
    slingStrap: String
});
module.exports = bagAttribute;
