var mongoose = require('mongoose');
var PincodeDetailsSchema = new mongoose.Schema({
    
        City: String,
        Pincode: String,
        HasCod: String,
        HasPrepaid: String,
        HasReverse: String,
        ShiprocketDeliveryZone: String,
        RoutingCode: String,
        State: String,
        TAT: String
    
    
}); 
 
const PincodeDetails = mongoose.model('pincodedetail', PincodeDetailsSchema);
module.exports = PincodeDetails;