
var mongoose = require('mongoose');


const kurthiAttribute = new mongoose.Schema({
    fitType: String,
    neckLine: String,
    sleeveLength: String,
    kurthiType: String,
    stichType: String,
    kurtaSet: String,
    acrossShoulderInches: String,
    bustInches: String,
    chestInches: String,
    frontLengthInches: String,
    toFitBustInches: String,
    sleeveLengthInches: String,
    toFitWaist: String,
    waistInchese: String
});
module.exports = kurthiAttribute;
