
var mongoose = require('mongoose');


const shirtsAttribute = new mongoose.Schema({
    hemline: String,
    placket: String,
    placketLength: String,
    cuff: String,
    pocketType: String,
    numberOfPockets: String,
    collarStyle: String,
    neck: String,
    sleeveLength: String,
    occasion: String,
    acrossShoulderInches: String,
    bustInches: String,
    chestInches: String,
    frontLengthInches: String,
    toFitBustInches: String,
    SleeveLengthInches: String,
    toFitWaistInches: String,
    waistInches: String

});
module.exports = shirtsAttribute;
