var mongoose = require('mongoose');


const OptionValueSchema = new mongoose.Schema({
    optionValueName: String,
    sortOrder: Number
});
module.exports = OptionValueSchema;
