var mongoose = require('mongoose');
const readyToWearModelSchema = new mongoose.Schema({
    serviceName: String,
    price: Number,
    discount: Number,
    measurement: [String],
    selectedCategoryID: String,
    selectedCategory: String,
    /* sizeChartName: String, */
    sizeChartCM: String,
    sizeChartInches: String,
});
const readyToWear = mongoose.model('readytowear', readyToWearModelSchema);
module.exports = readyToWear;

