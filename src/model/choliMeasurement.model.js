var mongoose = require('mongoose');
var value = require('./blouseMeasurementValue.model');
var style = require('./blouseMeasurementStyle.model');
const Choli = new mongoose.Schema({
    aroundBust: [value],
    aroundAboveWaist: [value],
    choliLength: [value],
    sleeveStyle: [style],
    frontNeckStyle: [style],
    backNeckStyle: [style],
    choliClosingSide: [String],
    choliClosingWith: [String],
    lining: [String]
});

module.exports = Choli;