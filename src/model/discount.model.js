var mongoose = require('mongoose');

const DiscountSchema = new mongoose.Schema({
    discount: { type: Number, default: 0 } 
});

module.exports  = DiscountSchema;
/* const size = mongoose.model('productvariant', sizeModelSchema);
module.exports = size; */