var mongoose = require('mongoose');

const FilterCreateSchema = new mongoose.Schema({
  
    filterOptionPriceValue: [{minPrice: Number, maxPrice: Number} ],
    filterOptionDiscountValue: [{minDiscount: Number, maxDiscount: Number}],
    filterDispatchValue: [{day: Number}]
 /*  dispatchFilter: [{day: Number}],
  discountFilter: [{minDiscount: Number, maxDiscount: Number}] */
});

const Filter = mongoose.model('filteroption', FilterCreateSchema);
module.exports = Filter;