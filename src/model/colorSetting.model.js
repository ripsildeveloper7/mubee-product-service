var mongoose = require('mongoose');

const ColorSettingsSchema  = new mongoose.Schema({
    colorName: String,
    // colorCode: String,
    colorChoose: [String]
});


const ColorSetting = mongoose.model('color', ColorSettingsSchema);
module.exports = ColorSetting;