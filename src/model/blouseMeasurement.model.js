var mongoose = require('mongoose');
var value = require('./blouseMeasurementValue.model');
var style= require('./blouseMeasurementStyle.model');
const MeasurementSchema = new mongoose.Schema({
    superCategoryId: String,
    mainCategoryId: String,
    subCategoryId: String,
    measurementName: String,
    measurementType: [{ typeName: String, typeDescription: String}],
    aroundBust: [value],
    aroundAboveWaist: [value],
    blouseLength: [value],
    frontNeckStyle: [style],
    frontNeckDepth: [value],
    backNeckStyle: [style],
    backNeckDepth: [value],
    sleeveStyle: [style],
    sleeveLength: [value],
    aroundArm: [value],
    closingSide: [String],
    closingWith: [String],
    lining: [String],
    specialComment: String,
    creationDate: Date,
    modifiedDate: Date,
    price: Number,
    discount: Number
});
const measureMent = mongoose.model('measurement', MeasurementSchema);
module.exports = measureMent;