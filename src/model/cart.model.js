var mongoose = require('mongoose');
var Size = require('./size.model');

const cartSchema = new mongoose.Schema({
    userId: String,
    items: [{
      productId: {
        type: mongoose.Schema.Types.ObjectId,
                         ref: 'ProductSchema'  
                  },
             INTsku: String,
             qty: Number,
             tailoringService: Boolean,
             isMeasurement: Boolean,
             isUnstitched: Boolean,
             serviceId: String
             } ],
    /* tailoringService: [{type: String,
      mount: Number,
      selectedSize: String,
      discount: Number,
      serviceName: String}] */
  });

  const Cart = mongoose.model('cart', cartSchema);
  module.exports = Cart;
