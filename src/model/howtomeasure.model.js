var mongoose = require('mongoose');
var detail = require('./howToMeasureDetail.model');


const HowToMeasureSchema = new mongoose.Schema({
    title: String,
    imageName: String,
    superCategoryId: String,
    mainCategoryId: String,
    subCategoryId: String,
    superCategoryName: String,
    mainCategoryName: String,
    subCategoryName: String,
    name: String,
    detail: [detail]

});


const HowToMeasure = mongoose.model('howtomeasure', HowToMeasureSchema);
module.exports = HowToMeasure;