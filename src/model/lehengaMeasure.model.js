var mongoose = require('mongoose');
var value = require('./blouseMeasurementValue.model');
var style = require('./blouseMeasurementStyle.model');
const Lehenga = new mongoose.Schema({
    aroundWaist: [value],
    aroundHip: [value],
    lehengaLength: [value],
    lehengaClosingSide: [String],
    lehengaClosingWith: [String]
});

module.exports = Lehenga;