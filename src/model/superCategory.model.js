var mongoose = require('mongoose');
var MainCategory = require('./mainCategory.model');
var tailoringService = require('./tailoringService.model');
var FieldAttributeValue = require('./../model/fieldAttributeValue');

const SuperCategorySchema = new mongoose.Schema({
  categoryName: String,
  categoryDescription: String,
  mainCategory: [MainCategory],
  sortOrder: Number,
  status: String,
  categoryImageName: String,
  isTailoringService: Boolean,
  tailoringService: [tailoringService],
  attribute: [{fieldName: String, fieldType: String, sortOrder: Number, fieldSetting: String, fieldEnable: Boolean,
               fieldEnableValue: Boolean, fieldValue: [FieldAttributeValue] }]
});

const Category = mongoose.model('category', SuperCategorySchema);
module.exports = Category;