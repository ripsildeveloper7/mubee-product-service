var mongoos = require('mongoose');
var choli = require('../model/choliMeasurement.model');
var lehenga = require('../model/lehengaMeasure.model')
const LehengaMeasurementSchema = new mongoos.Schema({
    superCategoryId: String,
    measurementName: String,
    measurementType: [{ typeName: String, typeDescription: String}],
    creationDate: Date,
    modifiedDate: Date,
    price: Number,
    discount: Number,
    choliMeasurement: [choli],
    lehengaMeasurement: [lehenga]
});

var Lehenga = mongoos.model('lehengameasurement', LehengaMeasurementSchema);

module.exports = Lehenga;