'use strict';

var productMgr = require('./product/productMgr');
var productEditMgr = require('./product-edit/productEditMgr');
var excelMgr = require('./uploadExcel/excelMgr');
var upload = require('../config/multer.config');
var recentProductMgr = require('./recentProduct/recentProductMgr');
var tailoringMgr = require('./tailoringService/tailoringMgr');
var InventoryMgr = require('./inventory-management/inventoryMgr');

module.exports = function (app) {
  app.route('/addproduct')
    .post(productMgr.addProduct); // add product details
  app.route('/getchildproduct')
    .get(productMgr.getChildProduct); // add product details
  app.route('/uploadproduct')
    .post(productMgr.uploadProduct); // upload product details

    app.route('/updatebulkupload')
    .post(productMgr.updateBulkUpload);
    app.route('/parentproductimageupdate/:id')
    .put(productMgr.parentProudctImageUpdate);
    app.route('/childproductimageupdate/:id/:childId')
    .put(productMgr.childProudctImageUpdate);
  /*  app.route('/productimage/:id')
     .put(upload.array("uploads[]"), productMgr.addProductImage); */ // Upload Product Image
  app.route('/editproductcategory/:id')
    .put(productMgr.editProductCategory);
  app.route('/editproductbrand/:id')
    .put(productMgr.editProductBrand);
  app.route('/addsizevariant/:id')
    .put(productMgr.addSizeVariant);
  app.route('/editproductvariant/:id/productvariant/:productVariantId')
    .put(productMgr.editProductVariant);
  app.route('/product') // all product view
    .get(productMgr.getProduct);
    app.route('/allproductforadmin') // all product view
    .put(productMgr.getAllProductForAdmin);

  /* app.route('/productforui') // all product view for UI
    .get(productMgr.getProductForUI); */

  app.route('/productsingle/:id') // single product view
    .get(productMgr.getSingleProduct);

  app.route('/deleteproduct/:id') // delete product
    .delete(productMgr.deleteProduct);
  app.route('/editproductinfo/:id') // edit productinfo edit
    .put(productMgr.editProductInfo);
  app.route('/editproductseo/:id')
    .put(productMgr.editProductSeo); // edit seo edit
  

  app.route('/getproductpricerange')
    .post(productMgr.getProductPriceRange);
  /* app.route('/editproductimage/:id') // edit product image
    .put(upload.array("uploads[]"), productMgr.updateProductImage); */
  app.route('/deletesingleimage/:id/imagename/:name') // delete single image
    .delete(productMgr.deleteSingleProductImage); // delete product Variant
  app.route('/deleteproductvariant/:id/productvariant/:productVariantId')
    .delete(productMgr.deleteProductVariant);
  /*  app.route('/deletesingleimage/:id')
     .post(productMgr.deleteSingleProductImage); */
  app.route('/sortproductbydate')
    .post(productMgr.sortProductByDate); // sort product By Date  

  app.route('/sortproductbyname')
    .post(productMgr.sortProductByName); // sort product By Name

  app.route('/productoverallcount')
    .get(productMgr.productOverallCount);
  app.route('/singleproductwithbrand/:id')
    .get(productMgr.getSingleProductWithBrand);
  app.route('/allmaincategory/:maincatid')
    .put(productMgr.getAllProductMainCategory);
  app.route('/allsupercategory/:supercatid')
    .put(productMgr.getAllProductSuperCategory);
  app.route('/allsubcategory/:subcatid')
    .put(productMgr.getAllProductSubCategory);
  app.route('/getproductforrow')
    .get(productMgr.getProductForRow);

  app.route('/getproductbyiddemo/:id')
    .get(productMgr.getProductByIdDemo);  

  app.route('/brandimage/:id')
  app.route('/excel/:attributeId')
  .put(upload.single('excel'), excelMgr.uploadSingleExcel);
  /*     app.route('/relatedproducts/:stylecode/product/:id')
        .get(productMgr.relatedProducts);

    app.route('/product/:productId')
        .get(productMgr.getProductById);

    app.route('/productimages/:skuCode')
        .put(productMgr.createProductImage);

        app.route('/product/:id/region/:regionid')
        .put(productMgr.editRegionDetails);

        app.route('/mfdqty/:id')
        .put(productMgr.editQtyDetails);
 */

  app.route('/productimage/:id/childimage/:childid')
    .put(productEditMgr.uploadMultiImage);
  app.route('/editproductimage/:id') // edit product image
    .put(productEditMgr.uploadMultiImageEdit);
  /* app.route('/editproductdiscount/:id')
    .put(productMgr.editProductDiscount); */
    app.route('/editchildattribute/:id/:childId')
    .put(productEditMgr.editChildAttribute);
    app.route('/editproductparentinfo/:id')
    .put(productEditMgr.editParentProductInfo);
  app.route('/editproductchildinfo/:id/:childId')
    .put(productEditMgr.editChildProductInfo);
    
  app.route('/getproductchild/:id')
  .get(productMgr.getProductChild);


  app.route('/updatepublishproduct')
  .put(productMgr.updatePublishProduct);
  app.route('/updateunpublishproduct')
  .put(productMgr.UpdateUnPublishProduct);
  /* app.route('/editproductdiscount/:id')
    .put(productEditMgr.editProductDiscount); // edit discount edit
    app.route('/editproductchildinfo/:id/:childId')
    .put(productEditMgr.editChildProductInfo); */
    app.route('/getpromotionproduct')
    .post(productMgr.getPromotionProduct);  
    app.route('/searchproduct')
    .post(productMgr.searchProduct);
// Vendor

app.route('/updatequentitybyvendor/:id/:childId')
    .post(productEditMgr.updateQuantityByVendor);

// Recent Product
app.route('/addrecentproduct')
    .post(recentProductMgr.addrecentProduct);
app.route('/getrecentproductbyuser/:id')
    .get(recentProductMgr.getRecentProductByUser);    
app.route('/getrecentproduct/:id')
    .get(recentProductMgr.getRecentProudct);
app.route('/getproductforlocalrecent')
    .post(recentProductMgr.getProductForLocalStorage);            

app.route('/getcategory/:supId/attribute/:attributeId/attributefield/:attributefieldId')
    .get(productMgr.getAttributeProduct);  
    app.route('/allproductbybrand/:brandid')
    .get(productMgr.getAllProductBrand);  

    // tailoring service

    app.route('/createreadytowear')
    .post(tailoringMgr.createReadyToWear); 
    
    app.route('/updatereadytowear/:id')
    .put(tailoringMgr.updateReadyToWear); 

    app.route('/getallreadytowear')
    .get(tailoringMgr.getReadyToWear);  

    app.route('/getsinglereadytowear/:id')
    .get(tailoringMgr.getSingleReadyToWear);   
    
    app.route('/deletereadytowear/:id')
    .delete(tailoringMgr.deleteReadyToWear);  
    
    app.route('/addsizechartcmnameready/:id')
    .put(tailoringMgr.addSizeCharCMName);

    app.route('/addsizechartinchesnameready/:id')
    .put(tailoringMgr.addSizeCharInchesName);
    app.route('/updatesizecharcmnameready/:id')
    .put(tailoringMgr.updateSizeChartCM);

    app.route('/updatesizecharinchesready/:id')
    .put(tailoringMgr.updateSizeChartinches);

    app.route('/getyoumayalsolike')
    .post(productMgr.youMayAlsoLike);  

    // How to measurement

    app.route('/createmeasurement')
    .post(tailoringMgr.createMeasurement);

    app.route('/addaroundbustimagename/:id')
    .put(tailoringMgr.addAroundBustImageName);
    app.route('/addaroundabovewaistimagename/:id')
    .put(tailoringMgr.addAroundAboveWaistImageName);
    app.route('/addblouselengthimagename/:id')
    .put(tailoringMgr.addBlouseLengthImageName);
    app.route('/addfrontneckdepthimagename/:id')
    .put(tailoringMgr.addFrontNeckDepthImageName);
    app.route('/addbackneckdepthimagename/:id')
    .put(tailoringMgr.addBackNeckDepthImageName);
    app.route('/addsleevelengthimagename/:id')
    .put(tailoringMgr.addSleeveLengthImageName);
    app.route('/addaroundarmimagename/:id')
    .put(tailoringMgr.addAroundArmImageName);
    app.route('/getallmeasurement')
    .get(tailoringMgr.getAllMeasurement);
    app.route('/getsinglemeasurement/:id')
    .get(tailoringMgr.getSingleMeasurement);
    app.route('/addfrontneckstylemeasurement/:id')
    .put(tailoringMgr.addFrontNeckStyleMeasurement);
    app.route('/addfrontneckstylemeasurementname/:id/:styleId')
    .put(tailoringMgr.addFrontNeckStyleMeasurementName);
    app.route('/addbackneckstylemeasurement/:id')
    .put(tailoringMgr.addBackNeckStyleMeasurement);
    app.route('/addbackneckstylemeasurementname/:id/:styleId')
    .put(tailoringMgr.addBackNeckStyleMeasurementName);
    app.route('/addsleevestylemeasurement/:id')
    .put(tailoringMgr.addSleeveStyleMeasurement);
    app.route('/addsleevestylemeasurementname/:id/:styleId')
    .put(tailoringMgr.addSleeveStyleMeasurementName);
    app.route('/updatemeasurementdata/:id')
    .put(tailoringMgr.updateMeasurementData);
    app.route('/removefrontneckstyle/:id/:styleId')
    .delete(tailoringMgr.removeFrontNeckStyle);
    app.route('/removebackneckstyle/:id/:styleId')
    .delete(tailoringMgr.removeBackNeckStyle);
    app.route('/removesleevestyle/:id/:styleId')
    .delete(tailoringMgr.removeSleeveStyle);
    app.route('/getselectedsupercategorymeasurement/:id')
    .get(tailoringMgr.getSelectedSuperCategoryTailoringService);

    // Kameez Measurement 

    app.route('/createkameezmeasurement')
    .post(tailoringMgr.createKameezMeasurement);
    app.route('/addkameezaroundbustimagename/:id')
    .put(tailoringMgr.addKameezAroundBustImageName);
    app.route('/addkameezaroundabovewaistimagename/:id')
    .put(tailoringMgr.addKameezAroundAboveWaistImageName);
    app.route('/addkameezaroundhipimagename/:id')
    .put(tailoringMgr.addKameezAroundHipImageName);
    app.route('/addkameezlengthimagename/:id')
    .put(tailoringMgr.addKameezLengthImageName);
    app.route('/addkameezfrontneckdepthimagename/:id')
    .put(tailoringMgr.addKameezFrontNeckDepthImageName);
    app.route('/addkameezbackneckdepthimagename/:id')
    .put(tailoringMgr.addKameezBackNeckDepthImageName);
    app.route('/addkameezsleevelengthimagename/:id')
    .put(tailoringMgr.addKameezSleeveLengthImageName);
    app.route('/addkameezaroundarmimagename/:id')
    .put(tailoringMgr.addKameezAroundArmImageName);
    app.route('/addkameezfrontneckstylemeasurement/:id')
    .put(tailoringMgr.addKameezFrontNeckStyleMeasurement);
    app.route('/addkameezfrontneckstylemeasurementname/:id/:styleId')
    .put(tailoringMgr.addKameezFrontNeckStyleMeasurementName);
    app.route('/addkameezbackneckstylemeasurement/:id')
    .put(tailoringMgr.addKameezBackNeckStyleMeasurement);
    app.route('/addkameezbackneckstylemeasurementname/:id/:styleId')
    .put(tailoringMgr.addKameezBackNeckStyleMeasurementName);
    app.route('/addkameezsleevestylemeasurement/:id')
    .put(tailoringMgr.addKameezSleeveStyleMeasurement);
    app.route('/addkameezsleevestylemeasurementname/:id/:styleId')
    .put(tailoringMgr.addKameezSleeveStyleMeasurementName);
    app.route('/addkameezbottomstylemeasurement/:id')
    .put(tailoringMgr.addKameezBottomStyleMeasurement);
    app.route('/addkameezbottomstylemeasurementname/:id/:styleId')
    .put(tailoringMgr.addKameezBottomStyleMeasurementName);
    app.route('/addkameezaroundwaistimagename/:id')
    .put(tailoringMgr.addKameezAroundWaistImageName);
    app.route('/addkameezaroundthighimagename/:id')
    .put(tailoringMgr.addKameezAroundThighImageName);
    app.route('/addkameezaroundkneeimagename/:id')
    .put(tailoringMgr.addKameezAroundKneeImageName);
    app.route('/addkameezaroundcalfimagename/:id')
    .put(tailoringMgr.addKameezAroundCalfImageName);
    app.route('/addkameezaroundbottomimagename/:id')
    .put(tailoringMgr.addKameezAroundBottomImageName);
    app.route('/addkameezbottomlengthimagename/:id')
    .put(tailoringMgr.addKameezBottomLengthImageName);
    app.route('/removekameezfrontneckstyle/:id/:styleId')
    .delete(tailoringMgr.removeKameezFrontNeckStyle);
    app.route('/removekameezbackneckstyle/:id/:styleId')
    .delete(tailoringMgr.removeKameezBackNeckStyle);
    app.route('/removekameezsleevestyle/:id/:styleId')
    .delete(tailoringMgr.removeKameezSleeveStyle);
    app.route('/removekameezbottomstyle/:id/:styleId')
    .delete(tailoringMgr.removeKameezBottomStyle);
    app.route('/getallkameezmeasurement')
    .get(tailoringMgr.getAllKameezMeasurement);
    app.route('/getsinglekameezmeasurement/:id')
    .get(tailoringMgr.getSingleKameezMeasurement);
    app.route('/updatekameezmeasurement/:id')
    .put(tailoringMgr.updateKameezMeasurement);
    app.route('/getselectedsupercategorykameezmeasurement/:id')
    .get(tailoringMgr.getSelectedSuperCategoryKameezMeasurement);

    app.route('/deletemultiproduct')
    .post(productMgr.multiDeleteProduct);

    app.route('/getproductbyvendor/:id')
    .get(productMgr.getProductByVendor);

    app.route('/getproductbybrand/:id')
    .get(productMgr.getProductByBrand);
    
    app.route('/pagination')
    .put(productMgr.pagination);
    
    app.route('/createlehengameasurement')
    .post(tailoringMgr.createLehengaMeasurement);

    app.route('/addlehengaaroundbustimagename/:id')
    .put(tailoringMgr.addLehengaAroundBustImageName);

    app.route('/addlehengaaroundabovewaistimagename/:id')
    .put(tailoringMgr.addLehengaAroundAboveWaistImageName);

    app.route('/addcholilengthimagename/:id')
    .put(tailoringMgr.addCholiLengthImageName);

    app.route('/addcholisleevestyle/:id')
    .put(tailoringMgr.addCholiSleeveStyleMeasurement);

    app.route('/addcholisleevestyleimagename/:id/:styleId')
    .put(tailoringMgr.addCholiSleeveStyleMeasurementName);

    app.route('/addlehengafrontneckstyle/:id')
    .put(tailoringMgr.addLehengaFrontNeckStyleMeasurement);

    app.route('/addlehengafrontneckstyleimagename/:id/:styleId')
    .put(tailoringMgr.addLehengaFrontNeckStyleMeasurementName);

    app.route('/addlehengabackneckstyle/:id')
    .put(tailoringMgr.addLehengaBackNeckStyleMeasurement);

    app.route('/addlehengabackneckstyleimagename/:id/:styleId')
    .put(tailoringMgr.addLehengaBackNeckStyleMeasurementName);

    app.route('/addlehengaaroundwaistimagename/:id')
    .put(tailoringMgr.addLehengaAroundWaistImageName);

    app.route('/addlehengaaroundhipimagename/:id')
    .put(tailoringMgr.addLehengaAroundHipImageName);

    app.route('/addlehengalengthimagename/:id')
    .put(tailoringMgr.addLehengaLengthImageName);

    app.route('/removelehengafrontneckstyle/:id/:styleId')
    .delete(tailoringMgr.removeLehengaFrontNeckStyle);

    app.route('/removelehengabackneckstyle/:id/:styleId')
    .delete(tailoringMgr.removeLehengaBackNeckStyle);

    app.route('/removelehengasleevetyle/:id/:styleId')
    .delete(tailoringMgr.removeLehengaSleeveStyle);

    app.route('/getlehengameasurement')
    .get(tailoringMgr.getLehengaMeasurement);

    app.route('/getsinglelehengameasurement/:id')
    .get(tailoringMgr.getSingleLehengaMeasurement);

    app.route('/updatelehengameasurement/:id')
    .put(tailoringMgr.updateLehengaMeasurement);
    app.route('/supercategoryexceldropdown/:supId')
    .get(excelMgr.superCategoryExceldropdown);
    app.route('/supercategoryexceldropdown/:supId/main/:mainId')
    .get(excelMgr.mainCategoryExceldropdown);
    app.route('/supercategoryexceldropdown/:supId/main/:mainId/sub/:subId')
    .get(excelMgr.subCategoryExceldropdown);

    app.route('/getlehengabycategory/:id')
    .get(tailoringMgr.getLehengaMeasurementByCategory);
    app.route('/getsupercategoryandfilter/:supId')
    .get(productMgr.getSupercategoryAndFilter);
    app.route('/getsubcategoryandfilter/:subId')
    .get(productMgr.getSubcategoryAndFilter);

    app.route('/getmaincategoryandfilter/:mainId')
    .get(productMgr.getMaincategoryAndFilter);

    app.route('/getSupCount/:id')
    .get(productMgr.getCatCount);

    app.route('/getMainCount/:maincatid')
    .get(productMgr.getMainCount);
    
    app.route('/getSubCount/:subid')
    .get(productMgr.getSubCount);

    //download
    app.route('/getproductbysupercategory/:id')
    .get(productMgr.getProductBySuperCategory);
    app.route('/getprdouctbysubcategory/:id')
    .get(productMgr.getProductBySubCategory);

    app.route('/getprdouctbymaincategory/:id')
    .get(productMgr.getProductByMainCategory);
    
    app.route('/getinventoryquantity')
    .get(productMgr.getInventoryQty);

    app.route('/getcategorywiseproductcount')
    .get(productMgr.getCategoryWiseProductCount);

    app.route('/getoutofstockqtycount')
    .get(productMgr.getOutOfStockQtyCount);

    //Inventory
    app.route('/updateinventorybyshipping')
    .post(InventoryMgr.updateInventoryByShipping);
}