var excelDA = require('./excelDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
var excelStart = require('../../config/excelStart.json');
var excelEnd = require('../../config/excelEnd.json');
var SuperCategory = require('../../model/superCategory.model');
const uuidv4 = require('uuid/v4');
var xl = require('excel4node');
exports.uploadSingleExcel = function (req, res) {
  try {
    const params = {
      Bucket: env.Bucket + '/' + 'excel' + '/' + req.params.attributeId, // create a folder and save the image
      Key: req.file.originalname,
      ACL: 'public-read',
      Body: req.file.buffer,
    };

    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        excelDA.uploadSingleExcel(req, data, res);
      }
    });
  } catch (error) {
    console.log(error);
  }
}

exports.superCategoryExceldropdown = function (req, res) {
  const excelCenter = {};
  SuperCategory.findOne({
    _id: req.params.supId
  }, function (err, supercategories) {
    if (err) {
      res.status(500).json(err);
    } else {
      var attributeSizeData = supercategories.attribute.find(function (detail) {

        return (detail.fieldSetting === 'Size');
      });
      var attributeColorData = supercategories.attribute.find(function (detail) {

        return (detail.fieldSetting === 'Color');
      });

      var attributeData = supercategories.attribute.filter(function (detail) {

        return (detail.fieldSetting !== 'Size') && (detail.fieldSetting !== 'Color');
      });
      for (const ite of attributeData) {
        if (!excelCenter[ite.fieldName]) {
          const subele = ite.fieldName;
          excelCenter[subele] = '';
        }
      }
      /* console.log(excelCenter); */
      var wb = new xl.Workbook();
      
      let excelStartArr = [];
      let excelEndArr = [];
      let excelCenterArr = [];
      for (let key in excelStart) {
        if (excelStart.hasOwnProperty(key)) {
          excelStartArr.push(key);
        }
      }
      for (let key in excelCenter) {
        if (excelCenter.hasOwnProperty(key)) {
          excelCenterArr.push(key);
        }
      }
      for (let key in excelEnd) {
        if (excelEnd.hasOwnProperty(key)) {
          excelEndArr.push(key);
        }
      }
      var excelAllHeader = excelCenterArr.length > 0 ? excelStartArr.concat(excelCenterArr, excelEndArr) : excelStartArr.concat(excelEndArr);
      var style = wb.createStyle({
        font: {
          color: '#FF0800',
          size: 12
        },
        numberFormat: '$#,##0.00; ($#,##0.00); -',
      });
      // Add Worksheets to the workbook
      var ws = wb.addWorksheet('Sheet1');
      /* {'hidden': true} */
      var ws2 = wb.addWorksheet('Sheet2', {'hidden': true});
      // ws2.cell(1, 1).string('Search');
      /* ws2.cell(2, 1).formulas('=IFERROR(SEARCH(INDIRECT(CELL("address")), M2),0)');
      ws2.cell(1, 2).string('Frequency');
      ws2.cell(2, 1).formulas('=IF(A2=0,"",COUNTIF($B$2:B2, ">0"))');
      ws2.cell(1, 3).string('Final List'); */
      /* =OFFSET(Sheet1!$D$2,,,COUNTIF(Sheet1!$D:$D,"*?")-1) */
      for (var i = 0; i < excelAllHeader.length; i++) {
        ws.cell(1, i + 1).string(excelAllHeader[i]);
        let remaining = i + 1;
        let aCharCode = 65;
        let columnName = '';
        while (remaining > 0) {
          let mod = (remaining - 1) % 26;
          columnName = String.fromCharCode(aCharCode + mod) + columnName;
          remaining = (remaining - 1 - mod) / 26;
        }
         if(excelAllHeader[i] == 'variation') {
         ws.addDataValidation({
           type: 'list',
           allowBlank: true,
           // prompt: 'Choose from dropdown',
           error: 'Invalid choice was chosen',
           showDropDown: true,
           sqref: columnName +"2:"+ columnName + "10",
           formulas: ['Parent, Child'],
         });
       } 
        switch (excelAllHeader[i]) {
          case 'variation':
            ws.addDataValidation({
              type: 'list',
              allowBlank: true,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ['Parent, Child'],
            });
            break;
          case 'variationType':
            ws.addDataValidation({
              type: 'list',
              allowBlank: true,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ['None, Size, Color, SizeColor']
            });
            break;
          case 'color':
              if (attributeColorData) {
                var colorData = attributeColorData.fieldValue.map(el => el.fieldAttributeValue)
                for (var j = 0; j < colorData.length; j++) {
                ws2.cell(j + 1, i + 1).string(colorData[j]);
              }
              /* ws.cell(2, i + 1).formula('=OFFSET(Sheet1!$M$1, 0, 0, COUNTA(Sheet2!$M:$M), 1)'); */
              
            ws.addDataValidation({
              type: 'list',
              allowBlank: 1,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              operator: 'equal',
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ["=sheet2!$" + columnName +"$1:$" + columnName + "$" + colorData.length]
            });
          }
          var myStyle = wb.createStyle({
            font: {
              bold: true,
              color: '00FF00',
            },
          });
          /* ws.cell(j + 1, 2).formula("=OFFSET(Sheet1!$M$2;0;0;COUNTA(Sheet2!$M:$M);1)"); */
          /* ws.cell(j + 1, 2).formula("=OFFSET(Sheet1!$M$2,0,0,MATCH(*,Sheet2!$M$1:$M$300,-1),1)"); */
            break;
         case 'sizeVariant':
            if (attributeSizeData) {
              var sizeData = attributeSizeData.fieldValue.map(el => el.fieldAttributeValue)
              for (var j = 0; j < sizeData.length; j++) {
              ws2.cell(j + 1, i + 1).string(sizeData[j]);
            }
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                imeMode: 'on',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                 // formulas: [attributeSizeData.fieldValue.map(el => el.fieldAttributeValue).toString()] 
                formulas: ["=sheet2!$" + columnName +"$1:$" + columnName + "$" + sizeData.length]
              });
            } else {
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                formulas: ["None"]
              });
            }
            break;
          case excelAllHeader[i]:
            var exceldata = attributeData.find(function (detail) {
              return (detail.fieldType === "Dropdown" &&  detail.fieldName == excelAllHeader[i] );
            });
            if (exceldata) {
              var filterValueData = exceldata.fieldValue.map(el => el.fieldAttributeValue)
              for (var j = 0; j < filterValueData.length; j++) {
              ws2.cell(j + 1, i + 1).string(filterValueData[j]);
            }
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                // formulas: [exceldata.fieldValue.map(el => el.fieldAttributeValue).toString()] 
                formulas: ["=sheet2!$" + columnName +"$1:$" + columnName + "$" + filterValueData.length]
              });
            }
            break; 
        }
      }
      /* const test = wb.write('excel.xlsx'); */
      wb.writeToBuffer().then(function (file_buffer) {
        var base64data =  Buffer.from(file_buffer).toString('base64');
        /* console.log(base64data); */
        res.status(200).json(base64data);
        
      });
      
      

  /*     let fileName = supercategories.categoryName + '.xlsx';

  //Replace the variable value with your bucket name
  let bucketName = 'excelfortemporarylogs/';

  wb.writeToBuffer().then(function (file_buffer) {

    var params = {
      Bucket: env.ExcelBucket + '/' + bucketName + supercategories._id,
      Key: fileName,
      Body: file_buffer
    };

    s3.putObject(params, function (err, pres) {
      if (err) {
        res.status(500).json(err);
      } else {

        const signedUrlExpireSeconds = 86400;// 1 Day
       let url =   s3.getSignedUrl('getObject', {
          Bucket: bucketName,
          Key: fileName,
          Expires: signedUrlExpireSeconds
        });
      }
    });
  }); */
  // Create a new instance of a Workbook class
}
});
}
exports.mainCategoryExceldropdown = function (req, res) {
  const excelCenter = {};
  SuperCategory.findOne({
    _id: req.params.supId
  }, function (err, supercategories) {
    if (err) {
      res.status(500).json(err);
    } else {
      var mainCat = supercategories.mainCategory.id(req.params.mainId)
      var attributeSizeData = mainCat.attribute.find(function (detail) {

        return (detail.fieldSetting === 'Size');
      });
      var attributeColorData = mainCat.attribute.find(function (detail) {

        return (detail.fieldSetting === 'Color');
      });

      var attributeData = mainCat.attribute.filter(function (detail) {

        return (detail.fieldSetting !== 'Size') && (detail.fieldSetting !== 'Color');
      });
      for (const ite of attributeData) {
        if (!excelCenter[ite.fieldName]) {
          const subele = ite.fieldName;
          excelCenter[subele] = '';
        }
      }
      /* console.log(excelCenter); */
      var wb = new xl.Workbook();
      let excelStartArr = [];
      let excelEndArr = [];
      let excelCenterArr = [];
      for (let key in excelStart) {
        if (excelStart.hasOwnProperty(key)) {
          excelStartArr.push(key);
        }
      }
      for (let key in excelCenter) {
        if (excelCenter.hasOwnProperty(key)) {
          excelCenterArr.push(key);
        }
      }
      for (let key in excelEnd) {
        if (excelEnd.hasOwnProperty(key)) {
          excelEndArr.push(key);
        }
      }
      var excelAllHeader = excelCenterArr.length > 0 ? excelStartArr.concat(excelCenterArr, excelEndArr) : excelStartArr.concat(excelEndArr);
      var style = wb.createStyle({
        font: {
          color: '#FF0800',
          size: 12
        },
        numberFormat: '$#,##0.00; ($#,##0.00); -',
      });
      // Add Worksheets to the workbook
      var ws = wb.addWorksheet('Sheet1');
      var ws2 = wb.addWorksheet('Sheet2',{'hidden': true});
      for (var i = 0; i < excelAllHeader.length; i++) {
        ws.cell(1, i + 1).string(excelAllHeader[i]);
        let remaining = i + 1;
        let aCharCode = 65;
        let columnName = '';
        while (remaining > 0) {
          let mod = (remaining - 1) % 26;
          columnName = String.fromCharCode(aCharCode + mod) + columnName;
          remaining = (remaining - 1 - mod) / 26;
        }
        /*  if(excelAllHeader[i] == 'variation'){
         ws.addDataValidation({
           type: 'list',
           allowBlank: true,
           // prompt: 'Choose from dropdown',
           error: 'Invalid choice was chosen',
           showDropDown: true,
           sqref: columnName +"2:"+ columnName + "10",
           formulas: ['Parent, Child'],
         });
         wb.write('Excel.xlsx');
       } */
        /* switch (excelAllHeader[i]) {
          case 'variation':
            ws.addDataValidation({
              type: 'list',
              allowBlank: true,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ['Parent, Child'],
            });
            break;
          case 'variationType':
            ws.addDataValidation({
              type: 'list',
              allowBlank: true,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ['None, Size, Color, SizeColor']
            });
            break;
          case 'color':
              if (attributeColorData) {
            ws.addDataValidation({
              type: 'list',
              allowBlank: true,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: [attributeColorData.fieldValue.map(el => el.fieldAttributeValue).toString()]
            });
          }
            break;

          case 'sizeVariant':
            if (attributeSizeData) {
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                formulas: [attributeSizeData.fieldValue.map(el => el.fieldAttributeValue).toString()]
              });
            }  else {
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                formulas: ["None"]
              });
            }
            break;
          case excelAllHeader[i]:
            var exceldata = attributeData.find(function (detail) {
              return (detail.fieldName == excelAllHeader[i]);
            })
            if (exceldata && exceldata.fieldType === "Dropdown") {
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                formulas: [exceldata.fieldValue.map(el => el.fieldAttributeValue).toString()]
              });
            }
            break;
        } */
        switch (excelAllHeader[i]) {
          case 'variation':
            ws.addDataValidation({
              type: 'list',
              allowBlank: true,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ['Parent, Child'],
            });
            break;
          case 'variationType':
            ws.addDataValidation({
              type: 'list',
              allowBlank: true,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ['None, Size, Color, SizeColor']
            });
            break;
         case 'color':
              if (attributeColorData) {
                var colorData = attributeColorData.fieldValue.map(el => el.fieldAttributeValue)
                for (var j = 0; j < colorData.length; j++) {
                ws2.cell(j + 1, i + 1).string(colorData[j]);
              }
            ws.addDataValidation({
              type: 'list',
              allowBlank: 1,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ["=sheet2!$" + columnName +"$1:$" + columnName + "$" + colorData.length]
            });
          }
            break;
         case 'sizeVariant':
            if (attributeSizeData) {
              var sizeData = attributeSizeData.fieldValue.map(el => el.fieldAttributeValue)
              for (var j = 0; j < sizeData.length; j++) {
              ws2.cell(j + 1, i + 1).string(sizeData[j]);
            }
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                /* formulas: [attributeSizeData.fieldValue.map(el => el.fieldAttributeValue).toString()] */
                formulas: ["=sheet2!$" + columnName +"$1:$" + columnName + "$" + sizeData.length]
              });
            } else {
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                formulas: ["None"]
              });
            }
            break;
          case excelAllHeader[i]:
            var exceldata = attributeData.find(function (detail) {
              return (detail.fieldType === "Dropdown" &&  detail.fieldName == excelAllHeader[i] );
            })
            if (exceldata) {
              var filterValueData = exceldata.fieldValue.map(el => el.fieldAttributeValue);
              for (var j = 0; j < filterValueData.length; j++) {
              ws2.cell(j + 1, i + 1).string(filterValueData[j]);
            }
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                /* formulas: [exceldata.fieldValue.map(el => el.fieldAttributeValue).toString()] */
                formulas: ["=sheet2!$" + columnName +"$1:$" + columnName + "$" + filterValueData.length]
              });
            }
            break;
        }
      }
      wb.writeToBuffer().then(function (file_buffer) {
        var base64data =  Buffer.from(file_buffer).toString('base64');
        /* console.log(base64data); */
        res.status(200).json(base64data);
        
      });
    }
  });
  // Create a new instance of a Workbook class

}

exports.subCategoryExceldropdown = function (req, res) {
  const excelCenter = {};
  SuperCategory.findOne({
    _id: req.params.supId
  }, function (err, supercategories) {
    if (err) {
      res.status(500).json(err);
    } else {
      var mainCat = supercategories.mainCategory.id(req.params.mainId)
      var subCat = mainCat.subCategory.id(req.params.subId);
      var attributeSizeData = subCat.attribute.find(function (detail) {
        return (detail.fieldSetting === 'Size');
      });
      var attributeColorData = subCat.attribute.find(function (detail) {

        return (detail.fieldSetting === 'Color');
      });

      var attributeData = subCat.attribute.filter(function (detail) {

        return (detail.fieldSetting !== 'Size') && (detail.fieldSetting !== 'Color');
      });
      for (const ite of attributeData) {
        if (!excelCenter[ite.fieldName]) {
          const subele = ite.fieldName;
          excelCenter[subele] = '';
        }
      }
      /* console.log(excelCenter); */
      var wb = new xl.Workbook();
      let excelStartArr = [];
      let excelEndArr = [];
      let excelCenterArr = [];
      for (let key in excelStart) {
        if (excelStart.hasOwnProperty(key)) {
          excelStartArr.push(key);
        }
      }
      for (let key in excelCenter) {
        if (excelCenter.hasOwnProperty(key)) {
          excelCenterArr.push(key);
        }
      }
      for (let key in excelEnd) {
        if (excelEnd.hasOwnProperty(key)) {
          excelEndArr.push(key);
        }
      }
      var excelAllHeader = excelCenterArr.length > 0 ? excelStartArr.concat(excelCenterArr, excelEndArr) : excelStartArr.concat(excelEndArr);
      var style = wb.createStyle({
        font: {
          color: '#FF0800',
          size: 12
        },
        numberFormat: '$#,##0.00; ($#,##0.00); -',
      });
      // Add Worksheets to the workbook
      var ws = wb.addWorksheet('Sheet1');
      var ws2 = wb.addWorksheet('Sheet2', {'hidden': true});
      for (var i = 0; i < excelAllHeader.length; i++) {
        ws.cell(1, i + 1).string(excelAllHeader[i]);
        let remaining = i + 1;
        let aCharCode = 65;
        let columnName = '';
        while (remaining > 0) {
          let mod = (remaining - 1) % 26;
          columnName = String.fromCharCode(aCharCode + mod) + columnName;
          remaining = (remaining - 1 - mod) / 26;
        }
        /*  if(excelAllHeader[i] == 'variation'){
         ws.addDataValidation({
           type: 'list',
           allowBlank: true,
           // prompt: 'Choose from dropdown',
           error: 'Invalid choice was chosen',
           showDropDown: true,
           sqref: columnName +"2:"+ columnName + "10",
           formulas: ['Parent, Child'],
         });
         wb.write('Excel.xlsx');
       } */
        /* switch (excelAllHeader[i]) {
          case 'variation':
            ws.addDataValidation({
              type: 'list',
              allowBlank: true,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ['Parent, Child'],
            });
            break;
          case 'variationType':
            ws.addDataValidation({
              type: 'list',
              allowBlank: true,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ['None, Size, Color, SizeColor']
            });
            break;
          case 'color':
              if (attributeColorData) {
            ws.addDataValidation({
              type: 'list',
              allowBlank: true,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: [attributeColorData.fieldValue.map(el => el.fieldAttributeValue).toString()]
            });
          }
            break;

          case 'sizeVariant':
            if (attributeSizeData) {
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                formulas: [attributeSizeData.fieldValue.map(el => el.fieldAttributeValue).toString()]
              });
            }  else {
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                formulas: ["None"]
              });
            }
            break;
          case excelAllHeader[i]:
            var exceldata = attributeData.find(function (detail) {
              return (detail.fieldName == excelAllHeader[i]);
            })
            if (exceldata && exceldata.fieldType === "Dropdown") {
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                formulas: [exceldata.fieldValue.map(el => el.fieldAttributeValue).toString()]
              });
            }
            break;
        } */
        switch (excelAllHeader[i]) {
          case 'variation':
            ws.addDataValidation({
              type: 'list',
              allowBlank: true,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ['Parent, Child'],
            });
            break;
          case 'variationType':
            ws.addDataValidation({
              type: 'list',
              allowBlank: true,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ['None, Size, Color, SizeColor']
            });
            break;
           case 'color':
              if (attributeColorData) {
                var colorData = attributeColorData.fieldValue.map(el => el.fieldAttributeValue)
                for (var j = 0; j < colorData.length; j++) {
                ws2.cell(j + 1, i + 1).string(colorData[j]);
              }
            ws.addDataValidation({
              type: 'list',
              allowBlank: 1,
              // prompt: 'Choose from dropdown',
              error: 'Invalid choice was chosen',
              showDropDown: true,
              sqref: columnName + "2:" + columnName + "1000",
              formulas: ["=sheet2!$" + columnName +"$1:$" + columnName + "$" + colorData.length]
            });
          }
            break;
           case 'sizeVariant':
            if (attributeSizeData) {
              var sizeData = attributeSizeData.fieldValue.map(el => el.fieldAttributeValue)
              for (var j = 0; j < sizeData.length; j++) {
              ws2.cell(j + 1, i + 1).string(sizeData[j]);
            }
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                // formulas: [attributeSizeData.fieldValue.map(el => el.fieldAttributeValue).toString()]
                formulas: ["=sheet2!$" + columnName +"$1:$" + columnName + "$" + sizeData.length]
              });
            } else {
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                formulas: ["None"]
              });
            }
            break;
            case excelAllHeader[i]:
            var exceldata = attributeData.find(function (detail) {
              return (detail.fieldType === "Dropdown" &&  detail.fieldName == excelAllHeader[i] );
            });
            if (exceldata && exceldata.fieldValue.length > 0) {
              var filterValueData = exceldata.fieldValue.map(el => el.fieldAttributeValue);
              for (var j = 0; j < filterValueData.length; j++) {
                console.log(filterValueData[j]);
              ws2.cell(j + 1, i + 1).string(filterValueData[j]);
            }
              ws.addDataValidation({
                type: 'list',
                allowBlank: true,
                // prompt: 'Choose from dropdown',
                error: 'Invalid choice was chosen',
                showDropDown: true,
                sqref: columnName + "2:" + columnName + "1000",
                // formulas: [exceldata.fieldValue.map(el => el.fieldAttributeValue).toString()]
                formulas: ["=sheet2!$" + columnName +"$1:$" + columnName + "$" + filterValueData.length]
              });
            }
            break;
        }
      }
      wb.writeToBuffer().then(function (file_buffer) {
        var base64data =  Buffer.from(file_buffer).toString('base64');
        res.status(200).json(base64data);
      });
    }
  });
  // Create a new instance of a Workbook class

}
// Create a reusable style





/*
exports.dropdownExcel = function (req, res) {

  // Create workbook & add worksheet
  const workbook = new excel.Workbook();
  const worksheet = workbook.addWorksheet('ExampleSheet');

  // add column headers
  worksheet.columns = [
    { header: 'Package', key: 'package_name' },
    { header: 'Author', key: 'author_name' }
  ];

  // Add row using key mapping to columns
  worksheet.addRow(
    { package_name: "ABC", author_name: "Author 1" },
    { package_name: "XYZ", author_name: "Author 2" }
  );

  // Add rows as Array values
  worksheet
    .addRow(["BCD", "Author Name 3"]);

  // Add rows using both the above of rows
  const rows = [
    ["FGH", "Author Name 4"],
    { package_name: "PQR", author_name: "Author 5" }
  ];


  worksheet
    .addRows(rows);

  // save workbook to disk
  workbook
    .xlsx
    .writeFile('sample1.xlsx')
    .then(() => {
      res.sendFile('./sample1.xlsx', function(err){
        console.log('---------- error downloading file: ' + err);
    });
    })
    .catch((err) => {
      console.log("err", err);
    });
} */