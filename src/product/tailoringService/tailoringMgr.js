var tailoringDA = require('./tailoringDA');

exports.createReadyToWear = function(req, res) {
    try {
        tailoringDA.createReadyToWear(req, res);
    } catch(error) {
        console.log(error);
    }
}

exports.updateReadyToWear = function(req, res) {
    try {
        tailoringDA.updateReadyToWear(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.getReadyToWear = function(req, res) {
    try {
        tailoringDA.getReadyToWear(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.getSingleReadyToWear = function(req, res) {
    try {
        tailoringDA.getSingleReadyToWear(req, res);
    } catch(error) {
        console.log(error);
    }
}

exports.deleteReadyToWear = function(req, res) {
    try {
        tailoringDA.deleteReadyToWear(req, res);
    } catch(error) {
        console.log(error);
    }
}

exports.addSizeCharCMName = function(req, res) {
    try {
        tailoringDA.addSizeCharCMName(req, res);
    } catch(error) {
        console.log(error);
    }
}

exports.addSizeCharInchesName = function(req, res) {
    try {
        tailoringDA.addSizeCharInchesName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.updateSizeChartCM = function(req, res) {
    try {
        tailoringDA.updateSizeChartCM(req, res);
    } catch(error) {
        console.log(error);
    }
}

exports.updateSizeChartinches = function(req, res) {
    try {
        tailoringDA.updateSizeChartinches(req, res);
    } catch(error) {
        console.log(error);
    }
}

exports.createSemiStitch = function(req, res) {
    try {
        tailoringDA.createSemiStitch(req, res);
    } catch(error) {
        console.log(error);
    }
}

exports.updateSemiStitch = function(req, res) {
    try {
        tailoringDA.updateSemiStitch(req, res);
    } catch(error) {
        console.log(error);
    }
}

exports.createMeasurement = function(req, res) {
    try {
        const currentDate = new Date();
        const date = currentDate.getDate();
        const month = currentDate.getMonth() + 1;
        const year = currentDate.getFullYear();
        const toDayDate = month + '/' + date + '/' + year;
        tailoringDA.createMeasurement(req, res, toDayDate);
    } catch(error) {
        console.log(error);
    }
}
exports.addAroundBustImageName = function(req, res) {
    try {
        tailoringDA.addAroundBustImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addAroundAboveWaistImageName = function(req, res) {
    try {
        tailoringDA.addAroundAboveWaistImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addBlouseLengthImageName = function(req, res) {
    try {
        tailoringDA.addBlouseLengthImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addFrontNeckDepthImageName = function(req, res) {
    try {
        tailoringDA.addFrontNeckDepthImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addBackNeckDepthImageName = function(req, res) {
    try {
        tailoringDA.addBackNeckDepthImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addSleeveLengthImageName = function(req, res) {
    try {
        tailoringDA.addSleeveLengthImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addAroundArmImageName = function(req, res) {
    try {
        tailoringDA.addAroundArmImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.getAllMeasurement = function(req, res) {
    try {
        tailoringDA.getAllMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.getSingleMeasurement = function(req, res) {
    try {
        tailoringDA.getSingleMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addFrontNeckStyleMeasurement = function(req, res) {
    try {
        tailoringDA.addFrontNeckStyleMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addFrontNeckStyleMeasurementName = function(req, res) {
    try {
        tailoringDA.addFrontNeckStyleMeasurementName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addBackNeckStyleMeasurement = function(req, res) {
    try {
        tailoringDA.addBackNeckStyleMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addBackNeckStyleMeasurementName = function(req, res) {
    try {
        tailoringDA.addBackNeckStyleMeasurementName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addSleeveStyleMeasurement = function(req, res) {
    try {
        tailoringDA.addSleeveStyleMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addSleeveStyleMeasurementName = function(req, res) {
    try {
        tailoringDA.addSleeveStyleMeasurementName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.updateMeasurementData = function(req, res) {
    try {
        const currentDate = new Date();
        const date = currentDate.getDate();
        const month = currentDate.getMonth() + 1;
        const year = currentDate.getFullYear();
        const toDayDate = month + '/' + date + '/' + year;
        tailoringDA.updateMeasurementData (req, res, toDayDate);
    } catch(error) {
        console.log(error);
    }
}
exports.removeFrontNeckStyle = function(req, res) {
    try {
        tailoringDA.removeFrontNeckStyle(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.removeBackNeckStyle = function(req, res) {
    try {
        tailoringDA.removeBackNeckStyle(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.removeSleeveStyle = function(req, res) {
    try {
        tailoringDA.removeSleeveStyle(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.getSelectedSuperCategoryTailoringService = function(req, res) {
    try {
        tailoringDA.getSelectedSuperCategoryTailoringService(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.createKameezMeasurement = function(req, res) {
    try {
        const currentDate = new Date();
        const date = currentDate.getDate();
        const month = currentDate.getMonth() + 1;
        const year = currentDate.getFullYear();
        const toDayDate = month + '/' + date + '/' + year;
        tailoringDA.createKameezMeasurement (req, res, toDayDate);
    } catch(error) {
        console.log(error);
    }
}

exports.addKameezAroundBustImageName = function(req, res) {
    try {
        tailoringDA.addKameezAroundBustImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezAroundAboveWaistImageName = function(req, res) {
    try {
        tailoringDA.addKameezAroundAboveWaistImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}

exports.addKameezAroundHipImageName = function(req, res) {
    try {
        tailoringDA.addKameezAroundHipImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezLengthImageName = function(req, res) {
    try {
        tailoringDA.addKameezLengthImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezFrontNeckDepthImageName = function(req, res) {
    try {
        tailoringDA.addKameezFrontNeckDepthImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezBackNeckDepthImageName = function(req, res) {
    try {
        tailoringDA.addKameezBackNeckDepthImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezSleeveLengthImageName = function(req, res) {
    try {
        tailoringDA.addKameezSleeveLengthImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezAroundArmImageName = function(req, res) {
    try {
        tailoringDA.addKameezAroundArmImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezFrontNeckStyleMeasurement = function(req, res) {
    try {
        tailoringDA.addKameezFrontNeckStyleMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezFrontNeckStyleMeasurementName = function(req, res) {
    try {
        tailoringDA.addKameezFrontNeckStyleMeasurementName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezBackNeckStyleMeasurement = function(req, res) {
    try {
        tailoringDA.addKameezBackNeckStyleMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezBackNeckStyleMeasurementName = function(req, res) {
    try {
        tailoringDA.addKameezBackNeckStyleMeasurementName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezSleeveStyleMeasurement = function(req, res) {
    try {
        tailoringDA.addKameezSleeveStyleMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezSleeveStyleMeasurementName = function(req, res) {
    try {
        tailoringDA.addKameezSleeveStyleMeasurementName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezBottomStyleMeasurement = function(req, res) {
    try {
        tailoringDA.addKameezBottomStyleMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezBottomStyleMeasurementName = function(req, res) {
    try {
        tailoringDA.addKameezBottomStyleMeasurementName(req, res);
    } catch(error) {
        console.log(error);
    }
}

exports.addKameezAroundWaistImageName = function(req, res) {
    try {
        tailoringDA.addKameezAroundWaistImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezAroundThighImageName = function(req, res) {
    try {
        tailoringDA.addKameezAroundThighImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezAroundKneeImageName = function(req, res) {
    try {
        tailoringDA.addKameezAroundKneeImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezAroundCalfImageName = function(req, res) {
    try {
        tailoringDA.addKameezAroundCalfImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezAroundBottomImageName = function(req, res) {
    try {
        tailoringDA.addKameezAroundBottomImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addKameezBottomLengthImageName = function(req, res) {
    try {
        tailoringDA.addKameezBottomLengthImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.removeKameezFrontNeckStyle = function(req, res) {
    try {
        tailoringDA.removeKameezFrontNeckStyle(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.removeKameezBackNeckStyle = function(req, res) {
    try {
        tailoringDA.removeKameezBackNeckStyle(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.removeKameezSleeveStyle = function(req, res) {
    try {
        tailoringDA.removeKameezSleeveStyle(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.removeKameezBottomStyle = function(req, res) {
    try {
        tailoringDA.removeKameezBottomStyle(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.getAllKameezMeasurement = function(req, res) {
    try {
        tailoringDA.getAllKameezMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.getSingleKameezMeasurement = function(req, res) {
    try {
        tailoringDA.getSingleKameezMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.updateKameezMeasurement = function(req, res) {
    try {
        var currentDate = new Date();
        var date = currentDate.getDate();
        var month = currentDate.getMonth();
        var year = currentDate.getFullYear();
        var toDate = month + '/' + date + '/' + year;
        tailoringDA.updateKameezMeasurement(req, res, toDate);
    } catch(error) {
        console.log(error);
    }
}
exports.getSelectedSuperCategoryKameezMeasurement = function(req, res) {
    try {
        tailoringDA.getSelectedSuperCategoryKameezMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}

exports.createLehengaMeasurement = function(req, res) {
    try {
        var currentDate = new Date();
        var date = currentDate.getDate();
        var month = currentDate.getMonth();
        var year = currentDate.getFullYear();
        var toDate = month + '/' + date + '/' + year;
        tailoringDA.createLehengaMeasurement(req, res, toDate);
    } catch(error) {
        console.log(error);
    }
}
exports.addLehengaAroundBustImageName = function(req, res) {
    try {
        tailoringDA.addLehengaAroundBustImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addLehengaAroundAboveWaistImageName = function(req, res) {
    try {
        tailoringDA.addLehengaAroundAboveWaistImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addCholiLengthImageName = function(req, res) {
    try {
        tailoringDA.addCholiLengthImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addCholiSleeveStyleMeasurement = function(req, res) {
    try {
        tailoringDA.addCholiSleeveStyleMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addCholiSleeveStyleMeasurementName = function(req, res) {
    try {
        tailoringDA.addCholiSleeveStyleMeasurementName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addLehengaFrontNeckStyleMeasurement = function(req, res) {
    try {
        tailoringDA.addLehengaFrontNeckStyleMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addLehengaFrontNeckStyleMeasurementName = function(req, res) {
    try {
        tailoringDA.addLehengaFrontNeckStyleMeasurementName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addLehengaBackNeckStyleMeasurement = function(req, res) {
    try {
        tailoringDA.addLehengaBackNeckStyleMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addLehengaBackNeckStyleMeasurementName = function(req, res) {
    try {
        tailoringDA.addLehengaBackNeckStyleMeasurementName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addLehengaAroundWaistImageName = function(req, res) {
    try {
        tailoringDA.addLehengaAroundWaistImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addLehengaAroundHipImageName = function(req, res) {
    try {
        tailoringDA.addLehengaAroundHipImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.addLehengaLengthImageName = function(req, res) {
    try {
        tailoringDA.addLehengaLengthImageName(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.removeLehengaFrontNeckStyle = function(req, res) {
    try {
        tailoringDA.removeLehengaFrontNeckStyle(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.removeLehengaBackNeckStyle = function(req, res) {
    try {
        tailoringDA.removeLehengaBackNeckStyle(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.removeLehengaSleeveStyle = function(req, res) {
    try {
        tailoringDA.removeLehengaSleeveStyle(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.getLehengaMeasurement = function(req, res) {
    try {
        tailoringDA.getLehengaMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.getSingleLehengaMeasurement = function(req, res) {
    try {
        tailoringDA.getSingleLehengaMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}
exports.updateLehengaMeasurement = function(req, res) {
    try {
        tailoringDA.updateLehengaMeasurement(req, res);
    } catch(error) {
        console.log(error);
    }
}

exports.getLehengaMeasurementByCategory = function(req, res) {
    try {
        tailoringDA.getLehengaMeasurementByCategory(req, res);
    } catch(error) {
        console.log(error);
    }
}
