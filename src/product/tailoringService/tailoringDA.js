var readyToWear = require('../../model/readyToWear.model');
var semiStitch = require('../../model/semiStitched.model');
var BlouseMeasurement = require('../../model/blouseMeasurement.model');
var KameezMeasurement = require('../../model/kameezMeasurement.model');
var LehengaMeasurement = require('../../model/lehengaMeasurement.model');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');

exports.createReadyToWear = function (req, res) {
    var create = new readyToWear(req.body);
    create.save(function (err, saveData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(saveData);
        }
    })
}

exports.updateReadyToWear = function (req, res) {
    readyToWear.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.serviceName = req.body.serviceName;
            findData.price = req.body.price;
            findData.discount = req.body.discount;
            findData.measurement = req.body.measurement;
            findData.selectedCategoryID = req.body.selectedCategoryID;
            findData.selectedCategory = req.body.selectedCategory;
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.getReadyToWear = function (req, res) {
    readyToWear.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.getSingleReadyToWear = function (req, res) {
    readyToWear.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}

exports.deleteReadyToWear = function (req, res) {
    readyToWear.findOneAndRemove({
        '_id': req.params.id
    }).select().exec(function (err, deleteData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(deleteData);
        }
    })
}
/* exports.addSizeChartName = function(req, res) {
    readyToWear.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.sizeChartName = req.body.sizeChartName;
            findData.save(function(err, saveData) {
                if(err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.updateSizeChartName = function(req, res) {
    readyToWear.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.sizeChartName === req.body.sizeChartName) {
                res.status(200).json(findData);
            } else {
                var s3 = new AWS.S3();
                s3.deleteObject({
                    Bucket: env.Bucket,
                    Key: 'images' + '/' + 'size' + '/' + req.params.id + '/' + findData.sizeChartName
                }, function(err, deleteData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        findData.sizeChartName = req.body.sizeChartName;
                        findData.save(function(err, updateData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(updateData);
                            }
                        })
                    }
                })
            }
        }
    })
}
 */
exports.createSemiStitch = function (req, res) {
    var create = new semiStitch(req.body);
    create.save(function (err, saveData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(saveData);
        }
    })
}

exports.updateSemiStitch = function (req, res) {
    semiStitch.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.serviceName = req.body.serviceName;
            findData.serviceValue = req.body.serviceValue;
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}

exports.addSizeCharCMName = function (req, res) {
    readyToWear.findOne({
        '_id': req.params.id
    }, function (err, data) {
        if (err) {
            res.status(500).send({
                message: "Some error occurred while retrieving notes."
            });
        } else {
            data.sizeChartCM = req.body.sizeChartCM;
            data.save(function (err, sizeguide) {
                if (err) {
                    console.log(err);
                    res.status(500).send({
                        message: "Some error occurred while retrieving notes."
                    });
                } else {
                    res.status(200).json(sizeguide);
                }
            });
        }
    });
}
exports.addSizeCharInchesName = function (req, res) {
    readyToWear.findOne({
        '_id': req.params.id
    }, function (err, data) {
        if (err) {
            res.status(500).send({
                message: "Some error occurred while retrieving notes."
            });
        } else {
            data.sizeChartInches = req.body.sizeChartInches;
            data.save(function (err, sizeguide) {
                if (err) {
                    console.log(err);
                    res.status(500).send({
                        message: "Some error occurred while retrieving notes."
                    });
                } else {
                    res.status(200).json(sizeguide);
                }
            });
        }
    });
}
exports.updateSizeChartCM = function (req, res) {
    readyToWear.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.sizeChartCM === req.body.sizeChartCM) {
                res.status(200).json(findData);
            } else {
                var s3 = new AWS.S3();
                s3.deleteObject({
                    Bucket: env.Bucket,
                    Key: 'images' + '/' + 'size' + '/' + req.params.id + '/' + findData.sizeChartCM
                }, function (err, deleteData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        findData.sizeChartCM = req.body.sizeChartCM;
                        findData.save(function (err, updateData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(updateData);
                            }
                        })
                    }
                })
            }
        }
    })
}
exports.updateSizeChartinches = function (req, res) {
    readyToWear.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.sizeChartInches === req.body.sizeChartInches) {
                res.status(200).json(findData);
            } else {
                var s3 = new AWS.S3();
                s3.deleteObject({
                    Bucket: env.Bucket,
                    Key: 'images' + '/' + 'size' + '/' + req.params.id + '/' + findData.sizeChartInches
                }, function (err, deleteData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        findData.sizeChartInches = req.body.sizeChartInches;
                        findData.save(function (err, updateData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(updateData);
                            }
                        })
                    }
                })
            }
        }
    })
}
// How to measurement

exports.createMeasurement = function (req, res, date) {
    var create = new BlouseMeasurement(req.body);
    create.creationDate = date;
    create.save(function (err, saveData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(saveData);
        }
    })
}

exports.addAroundBustImageName = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.aroundBust[0].imageName !== undefined && findData.aroundBust[0].imageName !== null) {
                if (findData.aroundBust[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.aroundBust[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.aroundBust[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.aroundBust[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}
exports.addAroundAboveWaistImageName = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.aroundAboveWaist[0].imageName !== undefined && findData.aroundAboveWaist[0].imageName !== null) {
                if (findData.aroundAboveWaist[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.aroundAboveWaist[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.aroundAboveWaist[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.aroundAboveWaist[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}
exports.addBlouseLengthImageName = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.blouseLength[0].imageName !== undefined && findData.blouseLength[0].imageName !== null) {
                if (findData.blouseLength[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.blouseLength[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.blouseLength[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.blouseLength[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}
exports.addFrontNeckDepthImageName = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.frontNeckDepth[0].imageName !== undefined && findData.frontNeckDepth[0].imageName !== null) {
                if (findData.frontNeckDepth[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.frontNeckDepth[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.frontNeckDepth[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.frontNeckDepth[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}
exports.addBackNeckDepthImageName = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.backNeckDepth[0].imageName !== undefined && findData.backNeckDepth[0].imageName !== null) {
                if (findData.backNeckDepth[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.backNeckDepth[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.backNeckDepth[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.backNeckDepth[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}
exports.addSleeveLengthImageName = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.sleeveLength[0].imageName !== undefined && findData.sleeveLength[0].imageName !== null) {
                if (findData.sleeveLength[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.sleeveLength[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.sleeveLength[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.sleeveLength[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}
exports.addAroundArmImageName = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.aroundArm[0].imageName !== undefined && findData.aroundArm[0].imageName !== null) {
                if (findData.aroundArm[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.aroundArm[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.aroundArm[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.aroundArm[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.getAllMeasurement = function (req, res) {
    BlouseMeasurement.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.getSingleMeasurement = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}

exports.addFrontNeckStyleMeasurement = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.frontNeckStyle.push(req.body.frontNeckStyle[0]);
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.addFrontNeckStyleMeasurementName = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var styleData = findData.frontNeckStyle.id(req.params.styleId);
            if (styleData.imageName !== undefined && styleData.imageName !== null) {
                if (styleData.imageName === req.body.imageName) {
                    BlouseMeasurement.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, findData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(findData);
                        }
                    })
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + styleData.imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            styleData.imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    BlouseMeasurement.findOne({
                                        '_id': req.params.id
                                    }).select().exec(function (err, findData) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(findData);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            } else {
                styleData.imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        BlouseMeasurement.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, findData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(findData);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.addBackNeckStyleMeasurement = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.backNeckStyle.push(req.body.backNeckStyle[0]);
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.addBackNeckStyleMeasurementName = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var styleData = findData.backNeckStyle.id(req.params.styleId);
            if (styleData.imageName !== undefined && styleData.imageName !== null) {
                if (styleData.imageName === req.body.imageName) {
                    BlouseMeasurement.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, findData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(findData);
                        }
                    })
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + styleData.imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            styleData.imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    BlouseMeasurement.findOne({
                                        '_id': req.params.id
                                    }).select().exec(function (err, findData) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(findData);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            } else {
                styleData.imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        BlouseMeasurement.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, findData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(findData);
                            }
                        })
                    }
                })
            }
        }
    })
}
exports.addSleeveStyleMeasurement = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.sleeveStyle.push(req.body.sleeveStyle[0]);
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.addSleeveStyleMeasurementName = function (req, res) {
    BlouseMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var styleData = findData.sleeveStyle.id(req.params.styleId);
            if (styleData.imageName !== undefined && styleData.imageName !== null) {
                if (styleData.imageName === req.body.imageName) {
                    BlouseMeasurement.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, findData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(findData);
                        }
                    })
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + styleData.imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            styleData.imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    BlouseMeasurement.findOne({
                                        '_id': req.params.id
                                    }).select().exec(function (err, findData) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(findData);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            } else {
                styleData.imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        BlouseMeasurement.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, findData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(findData);
                            }
                        })
                    }
                })
            }
        }
    })
}
exports.updateMeasurementData = function(req, res, date) {
    BlouseMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err); 
        } else {
            findData.superCategoryId = req.body.superCategoryId;
            findData.mainCategoryId = req.body.mainCategoryId;
            findData.subCategoryId = req.body.subCategoryId;
            findData.measurementName = req.body.measurementName;
            findData.measurementType = req.body.measurementType;
            findData.aroundBust = req.body.aroundBust;
            findData.aroundAboveWaist = req.body.aroundAboveWaist;
            findData.blouseLength = req.body.blouseLength;
            findData.frontNeckDepth = req.body.frontNeckDepth;
            findData.backNeckDepth = req.body.backNeckDepth;
            findData.sleeveLength = req.body.sleeveLength;
            findData.aroundArm = req.body.aroundArm;
            findData.closingSide = req.body.closingSide;
            findData.closingWith = req.body.closingWith;
            findData.lining = req.body.lining;
            findData.modifiedDate = date;
            findData.price = req.body.price;
            findData.discount = req.body.discount;
            findData.save(function(err, updateData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(updateData);
                }
            })
        }
    })
}
exports.removeFrontNeckStyle = function(req, res) {
    BlouseMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.frontNeckStyle.id(req.params.styleId).remove();
            findData.save(function(err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.removeBackNeckStyle = function(req, res) {
    BlouseMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.backNeckStyle.id(req.params.styleId).remove();
            findData.save(function(err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.removeSleeveStyle = function(req, res) {
    BlouseMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.sleeveStyle.id(req.params.styleId).remove();
            findData.save(function(err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.getSelectedSuperCategoryTailoringService = function(req, res) {
    BlouseMeasurement.find({superCategoryId: req.params.id}).select().exec(function(err, superData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(superData)
        }
    })
}

// Kameez Measurement

exports.createKameezMeasurement = function (req, res, date) {
    var create = new KameezMeasurement(req.body);
    create.creationDate = date;
    create.save(function (err, saveData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(saveData);
        }
    })
}
exports.addKameezAroundBustImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.kameezMeasurement[0].aroundBust[0].imageName !== undefined && findData.kameezMeasurement[0].aroundBust[0].imageName !== null) {
                if (findData.kameezMeasurement[0].aroundBust[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.kameezMeasurement[0].aroundBust[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.kameezMeasurement[0].aroundBust[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.kameezMeasurement[0].aroundBust[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addKameezAroundAboveWaistImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.kameezMeasurement[0].aroundAboveWaist[0].imageName !== undefined && findData.kameezMeasurement[0].aroundAboveWaist[0].imageName !== null) {
                if (findData.kameezMeasurement[0].aroundAboveWaist[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.kameezMeasurement[0].aroundAboveWaist[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.kameezMeasurement[0].aroundAboveWaist[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.kameezMeasurement[0].aroundAboveWaist[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addKameezAroundHipImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.kameezMeasurement[0].aroundHip[0].imageName !== undefined && findData.kameezMeasurement[0].aroundHip[0].imageName !== null) {
                if (findData.kameezMeasurement[0].aroundHip[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.kameezMeasurement[0].aroundHip[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.kameezMeasurement[0].aroundHip[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.kameezMeasurement[0].aroundHip[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addKameezLengthImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.kameezMeasurement[0].kameezLength[0].imageName !== undefined && findData.kameezMeasurement[0].kameezLength[0].imageName !== null) {
                if (findData.kameezMeasurement[0].kameezLength[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.kameezMeasurement[0].kameezLength[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.kameezMeasurement[0].kameezLength[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.kameezMeasurement[0].kameezLength[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addKameezFrontNeckDepthImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.kameezMeasurement[0].frontNeckDepth[0].imageName !== undefined && findData.kameezMeasurement[0].frontNeckDepth[0].imageName !== null) {
                if (findData.kameezMeasurement[0].frontNeckDepth[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.kameezMeasurement[0].frontNeckDepth[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.kameezMeasurement[0].frontNeckDepth[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.kameezMeasurement[0].frontNeckDepth[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addKameezBackNeckDepthImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.kameezMeasurement[0].backNeckDepth[0].imageName !== undefined && findData.kameezMeasurement[0].backNeckDepth[0].imageName !== null) {
                if (findData.kameezMeasurement[0].backNeckDepth[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.kameezMeasurement[0].backNeckDepth[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.kameezMeasurement[0].backNeckDepth[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.kameezMeasurement[0].backNeckDepth[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addKameezSleeveLengthImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.kameezMeasurement[0].sleeveLength[0].imageName !== undefined && findData.kameezMeasurement[0].sleeveLength[0].imageName !== null) {
                if (findData.kameezMeasurement[0].sleeveLength[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.kameezMeasurement[0].sleeveLength[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.kameezMeasurement[0].sleeveLength[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.kameezMeasurement[0].sleeveLength[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addKameezAroundArmImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.kameezMeasurement[0].aroundArm[0].imageName !== undefined && findData.kameezMeasurement[0].aroundArm[0].imageName !== null) {
                if (findData.kameezMeasurement[0].aroundArm[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.kameezMeasurement[0].aroundArm[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.kameezMeasurement[0].aroundArm[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.kameezMeasurement[0].aroundArm[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}
exports.addKameezFrontNeckStyleMeasurement = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.kameezMeasurement[0].frontNeckStyle.push(req.body.frontNeckStyle[0]);
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}

exports.addKameezFrontNeckStyleMeasurementName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var styleData = findData.kameezMeasurement[0].frontNeckStyle.id(req.params.styleId);
            if (styleData.imageName !== undefined && styleData.imageName !== null) {
                if (styleData.imageName === req.body.imageName) {
                    KameezMeasurement.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, findData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(findData);
                        }
                    })
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + styleData.imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            styleData.imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    KameezMeasurement.findOne({
                                        '_id': req.params.id
                                    }).select().exec(function (err, findData) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(findData);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            } else {
                styleData.imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        KameezMeasurement.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, findData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(findData);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.addKameezBackNeckStyleMeasurement = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.kameezMeasurement[0].backNeckStyle.push(req.body.backNeckStyle[0]);
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.addKameezBackNeckStyleMeasurementName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var styleData = findData.kameezMeasurement[0].backNeckStyle.id(req.params.styleId);
            if (styleData.imageName !== undefined && styleData.imageName !== null) {
                if (styleData.imageName === req.body.imageName) {
                    KameezMeasurement.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, findData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(findData);
                        }
                    })
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + styleData.imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            styleData.imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    KameezMeasurement.findOne({
                                        '_id': req.params.id
                                    }).select().exec(function (err, findData) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(findData);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            } else {
                styleData.imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        KameezMeasurement.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, findData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(findData);
                            }
                        })
                    }
                })
            }
        }
    })
}
exports.addKameezSleeveStyleMeasurement = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.kameezMeasurement[0].sleeveStyle.push(req.body.sleeveStyle[0]);
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.addKameezSleeveStyleMeasurementName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var styleData = findData.kameezMeasurement[0].sleeveStyle.id(req.params.styleId);
            if (styleData.imageName !== undefined && styleData.imageName !== null) {
                if (styleData.imageName === req.body.imageName) {
                    KameezMeasurement.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, findData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(findData);
                        }
                    })
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + styleData.imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            styleData.imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    KameezMeasurement.findOne({
                                        '_id': req.params.id
                                    }).select().exec(function (err, findData) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(findData);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            } else {
                styleData.imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        KameezMeasurement.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, findData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(findData);
                            }
                        })
                    }
                })
            }
        }
    })
}
//////
exports.addKameezBottomStyleMeasurement = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.bottomMeasurement[0].bottomStyle.push(req.body.bottomStyle[0]);
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.addKameezBottomStyleMeasurementName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var styleData = findData.bottomMeasurement[0].bottomStyle.id(req.params.styleId);
            if (styleData.imageName !== undefined && styleData.imageName !== null) {
                if (styleData.imageName === req.body.imageName) {
                    KameezMeasurement.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, findData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(findData);
                        }
                    })
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + styleData.imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            styleData.imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    KameezMeasurement.findOne({
                                        '_id': req.params.id
                                    }).select().exec(function (err, findData) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(findData);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            } else {
                styleData.imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        KameezMeasurement.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, findData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(findData);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.addKameezAroundWaistImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.bottomMeasurement[0].aroundWaist[0].imageName !== undefined && findData.bottomMeasurement[0].aroundWaist[0].imageName !== null) {
                if (findData.bottomMeasurement[0].aroundWaist[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.bottomMeasurement[0].aroundWaist[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.bottomMeasurement[0].aroundWaist[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.bottomMeasurement[0].aroundWaist[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addKameezAroundThighImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.bottomMeasurement[0].aroundThigh[0].imageName !== undefined && findData.bottomMeasurement[0].aroundThigh[0].imageName !== null) {
                if (findData.bottomMeasurement[0].aroundThigh[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.bottomMeasurement[0].aroundThigh[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.bottomMeasurement[0].aroundThigh[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.bottomMeasurement[0].aroundThigh[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addKameezAroundKneeImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.bottomMeasurement[0].aroundKnee[0].imageName !== undefined && findData.bottomMeasurement[0].aroundKnee[0].imageName !== null) {
                if (findData.bottomMeasurement[0].aroundKnee[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.bottomMeasurement[0].aroundKnee[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.bottomMeasurement[0].aroundKnee[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.bottomMeasurement[0].aroundKnee[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addKameezAroundCalfImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.bottomMeasurement[0].aroundCalf[0].imageName !== undefined && findData.bottomMeasurement[0].aroundCalf[0].imageName !== null) {
                if (findData.bottomMeasurement[0].aroundCalf[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.bottomMeasurement[0].aroundCalf[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.bottomMeasurement[0].aroundCalf[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.bottomMeasurement[0].aroundCalf[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}
exports.addKameezAroundBottomImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.bottomMeasurement[0].aroundBottom[0].imageName !== undefined && findData.bottomMeasurement[0].aroundBottom[0].imageName !== null) {
                if (findData.bottomMeasurement[0].aroundBottom[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.bottomMeasurement[0].aroundBottom[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.bottomMeasurement[0].aroundBottom[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.bottomMeasurement[0].aroundBottom[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addKameezBottomLengthImageName = function (req, res) {
    KameezMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.bottomMeasurement[0].bottomLength[0].imageName !== undefined && findData.bottomMeasurement[0].bottomLength[0].imageName !== null) {
                if (findData.bottomMeasurement[0].bottomLength[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.bottomMeasurement[0].bottomLength[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.bottomMeasurement[0].bottomLength[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.bottomMeasurement[0].bottomLength[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}
exports.removeKameezFrontNeckStyle = function(req, res) {
    KameezMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.kameezMeasurement[0].frontNeckStyle.id(req.params.styleId).remove();
            findData.save(function(err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.removeKameezBackNeckStyle = function(req, res) {
    KameezMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.kameezMeasurement[0].backNeckStyle.id(req.params.styleId).remove();
            findData.save(function(err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.removeKameezSleeveStyle = function(req, res) {
    KameezMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.kameezMeasurement[0].sleeveStyle.id(req.params.styleId).remove();
            findData.save(function(err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.removeKameezBottomStyle = function(req, res) {
    KameezMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.bottomMeasurement[0].bottomStyle.id(req.params.styleId).remove();
            findData.save(function(err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.getAllKameezMeasurement = function(req, res) {
    KameezMeasurement.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.getSingleKameezMeasurement = function(req, res) {
    KameezMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.updateKameezMeasurement = function(req, res, date) {
    KameezMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.superCategoryId = req.body.superCategoryId;
            findData.measurementName = req.body.measurementName;
            findData.measurementType = req.body.measurementType;
            findData.modifiedDate =  date,
            findData.price = req.body.price;
            findData.discount = req.body.discount;
            findData.kameezMeasurement = req.body.kameezMeasurement;
            findData.bottomMeasurement = req.body.bottomMeasurement;
            findData.save(function(err, updateData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(updateData);
                }
            })
        }
    })
}
exports.getSelectedSuperCategoryKameezMeasurement = function(req, res) {
    KameezMeasurement.find({superCategoryId: req.params.id}).select().exec(function(err, superData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(superData)
        }
    })
}

// Lehenga Measurement

exports.createLehengaMeasurement = function (req, res, date) {
    var create = new LehengaMeasurement(req.body);
    create.creationDate = date;
    create.save(function (err, saveData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(saveData);
        }
    })
}

exports.addLehengaAroundBustImageName = function (req, res) {
    LehengaMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.choliMeasurement[0].aroundBust[0].imageName !== undefined && findData.choliMeasurement[0].aroundBust[0].imageName !== null) {
                if (findData.choliMeasurement[0].aroundBust[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.choliMeasurement[0].aroundBust[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.choliMeasurement[0].aroundBust[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.choliMeasurement[0].aroundBust[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addLehengaAroundAboveWaistImageName = function (req, res) {
    LehengaMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.choliMeasurement[0].aroundAboveWaist[0].imageName !== undefined && findData.choliMeasurement[0].aroundAboveWaist[0].imageName !== null) {
                if (findData.choliMeasurement[0].aroundAboveWaist[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.choliMeasurement[0].aroundAboveWaist[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.choliMeasurement[0].aroundAboveWaist[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.choliMeasurement[0].aroundAboveWaist[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addCholiLengthImageName = function (req, res) {
    LehengaMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.choliMeasurement[0].choliLength[0].imageName !== undefined && findData.choliMeasurement[0].choliLength[0].imageName !== null) {
                if (findData.choliMeasurement[0].choliLength[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.choliMeasurement[0].choliLength[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.choliMeasurement[0].choliLength[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.choliMeasurement[0].choliLength[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addCholiSleeveStyleMeasurement = function (req, res) {
    LehengaMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.choliMeasurement[0].sleeveStyle.push(req.body.sleeveStyle[0]);
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.addCholiSleeveStyleMeasurementName = function (req, res) {
    LehengaMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var styleData = findData.choliMeasurement[0].sleeveStyle.id(req.params.styleId);
            if (styleData.imageName !== undefined && styleData.imageName !== null) {
                if (styleData.imageName === req.body.imageName) {
                    LehengaMeasurement.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, findData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(findData);
                        }
                    })
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + styleData.imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            styleData.imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    LehengaMeasurement.findOne({
                                        '_id': req.params.id
                                    }).select().exec(function (err, findData) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(findData);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            } else {
                styleData.imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        LehengaMeasurement.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, findData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(findData);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.addLehengaFrontNeckStyleMeasurement = function (req, res) {
    LehengaMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.choliMeasurement[0].frontNeckStyle.push(req.body.frontNeckStyle[0]);
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
} 

exports.addLehengaFrontNeckStyleMeasurementName = function (req, res) {
    LehengaMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var styleData = findData.choliMeasurement[0].frontNeckStyle.id(req.params.styleId);
            if (styleData.imageName !== undefined && styleData.imageName !== null) {
                if (styleData.imageName === req.body.imageName) {
                    LehengaMeasurement.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, findData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(findData);
                        }
                    })
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + styleData.imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            styleData.imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    LehengaMeasurement.findOne({
                                        '_id': req.params.id
                                    }).select().exec(function (err, findData) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(findData);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            } else {
                styleData.imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        LehengaMeasurement.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, findData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(findData);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.addLehengaBackNeckStyleMeasurement = function (req, res) {
    LehengaMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.choliMeasurement[0].backNeckStyle.push(req.body.backNeckStyle[0]);
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.addLehengaBackNeckStyleMeasurementName = function (req, res) {
    LehengaMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var styleData = findData.choliMeasurement[0].backNeckStyle.id(req.params.styleId);
            if (styleData.imageName !== undefined && styleData.imageName !== null) {
                if (styleData.imageName === req.body.imageName) {
                    LehengaMeasurement.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, findData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(findData);
                        }
                    })
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + styleData.imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            styleData.imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    LehengaMeasurement.findOne({
                                        '_id': req.params.id
                                    }).select().exec(function (err, findData) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(findData);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            } else {
                styleData.imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        LehengaMeasurement.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, findData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(findData);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.addLehengaAroundWaistImageName = function (req, res) {
    LehengaMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.lehengaMeasurement[0].aroundWaist[0].imageName !== undefined && findData.lehengaMeasurement[0].aroundWaist[0].imageName !== null) {
                if (findData.lehengaMeasurement[0].aroundWaist[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.lehengaMeasurement[0].aroundWaist[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.lehengaMeasurement[0].aroundWaist[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.lehengaMeasurement[0].aroundWaist[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addLehengaAroundHipImageName = function (req, res) {
    LehengaMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.lehengaMeasurement[0].aroundHip[0].imageName !== undefined && findData.lehengaMeasurement[0].aroundHip[0].imageName !== null) {
                if (findData.lehengaMeasurement[0].aroundHip[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.lehengaMeasurement[0].aroundHip[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.lehengaMeasurement[0].aroundHip[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.lehengaMeasurement[0].aroundHip[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.addLehengaLengthImageName = function (req, res) {
    LehengaMeasurement.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.lehengaMeasurement[0].lehengaLength[0].imageName !== undefined && findData.lehengaMeasurement[0].lehengaLength[0].imageName !== null) {
                if (findData.lehengaMeasurement[0].lehengaLength[0].imageName === req.body.imageName) {
                    res.status(200).json(findData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + findData.lehengaMeasurement[0].lehengaLength[0].imageName
                    }, function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            findData.lehengaMeasurement[0].lehengaLength[0].imageName = req.body.imageName;
                            findData.save(function (err, saveData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(saveData);
                                }
                            })
                        }
                    })
                }
            } else {
                findData.lehengaMeasurement[0].lehengaLength[0].imageName = req.body.imageName;
                findData.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            }

        }
    })
}

exports.removeLehengaFrontNeckStyle = function(req, res) {
    LehengaMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.choliMeasurement[0].frontNeckStyle.id(req.params.styleId).remove();
            findData.save(function(err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.removeLehengaBackNeckStyle = function(req, res) {
    LehengaMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.choliMeasurement[0].backNeckStyle.id(req.params.styleId).remove();
            findData.save(function(err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.removeLehengaSleeveStyle = function(req, res) {
    LehengaMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.choliMeasurement[0].sleeveStyle.id(req.params.styleId).remove();
            findData.save(function(err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}

exports.getLehengaMeasurement = function(req, res) {
    LehengaMeasurement.find({}).select().exec(function(err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(data);
        }
    })
}
exports.getSingleLehengaMeasurement = function(req, res) {
    LehengaMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(data);
        }
    })
}

exports.updateLehengaMeasurement = function(req, res) {
    LehengaMeasurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.superCategoryId = req.body.superCategoryId;
            findData.measurementName = req.body.measurementName;
            findData.measurementType = req.body.measurementType;
            findData.price = req.body.price;
            findData.discount = req.body.discount;
            findData.choliMeasurement = req.body.choliMeasurement;
            findData.lehengaMeasurement = req.body.lehengaMeasurement;
            findData.save(function(err, updateData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(updateData);
                }
            })
        }
    })
}

exports.getLehengaMeasurementByCategory = function(req, res) {
    LehengaMeasurement.find({'superCategoryId': req.params.id}).select().exec(function(err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(data);
        }
    })
}