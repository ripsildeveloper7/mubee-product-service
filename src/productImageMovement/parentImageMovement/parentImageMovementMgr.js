var parentImageMovementDA = require('./parentImageMovementDA');

exports.superCategoryImageCount = function (req, res) {
    try {
        parentImageMovementDA.superCategoryImageCount(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  exports.superCategoryImageUrlToSqs = function (req, res) {
    try {
        parentImageMovementDA.superCategoryImageUrlToSqs(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  
  
exports.mainCategoryImageCount = function (req, res) {
  try {
      parentImageMovementDA.mainCategoryImageCount(req, res);
  } catch (error) {
    console.log(error);
  }
}
  exports.mainCategoryImageUrlToSqs = function (req, res) {
    try {
        parentImageMovementDA.mainCategoryImageUrlToSqs(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  
exports.subCategoryImageCount = function (req, res) {
  try {
      parentImageMovementDA.subCategoryImageCount(req, res);
  } catch (error) {
    console.log(error);
  }
}
  exports.subCategoryImageUrlToSqs = function (req, res) {
    try {
        parentImageMovementDA.subCategoryImageUrlToSqs(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  
  exports.productImagesUrlToSqs = function (req, res) {
    try {
        parentImageMovementDA.productImagesUrlToSqs(req, res);
    } catch (error) {
      console.log(error);
    }
  }