var Product = require('../../model/product.model');
/* var sharp = require('sharp'); */
var imageResize = require('./../../config/imageResize');
var s3Env = require('../../config/s3.env');
var sqsEnv = require('../../config/sqs.env');
var mongooseUrl = require('../../config/mongoDatabase.config');
const sqs = require('../../config/sqs.config');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;


exports.superCategoryImageCount = function (req, res) { 
    Product.aggregate( [  { $unwind : "$productImage" }, {
        $match: {
            $and: [{ superCategoryId: ObjectId(req.params.superid)}, { "productImage.updateUrl": { $exists: false }}]
        }
      },  {
        $project: {
          superCategoryId: 1,
          mainCategoryId:1,
          subCategoryId:1,
          _id: 1,
          productImage: 1
        }
      },
    { '$count'    : "total" }
  ], function(err, findCountData) {
      if(err){
        res.status(500).send({
          "result": 'error occured while retreiving data'
        });
      } else {
         if(findCountData.length > 0) {
          var total = {
            total: findCountData[0].total
          };
          res.status(200).send(total);
        } else {
          var total = {
            total: 0
          };
          res.status(200).send(total);
        }
      }
    });
  }


exports.superCategoryImageUrlToSqs = function (req, res) {

    var page = parseInt(req.query.page) || 0; //for next page pass 1 here
      var limit = parseInt(req.query.limit) || 100
      var skip = page * limit;
        Product.aggregate( [ 
            { $unwind : "$productImage" },  {
                $match: {
                    $and: [{ superCategoryId: ObjectId(req.params.superid)}, { "productImage.updateUrl": { $exists: false }}]
                }
            },  {
              $project: {
                superCategoryId: 1,
                mainCategoryId:1,
                subCategoryId:1,
                variation: 1,
                _id: 1,
                productImage: 1
              }
            },
            {$skip: skip },
            {$limit: limit }
      ], function(err, findImageData) {
          if(err) {
            res.status(500).send({
              "result": 'error occured while retreiving data'
            });
          } else {
            if(findImageData.length > 0) {
              var size = 10; var arrayOfArrays = []; var arrayOfSqs = [];
              for(i = 0; i < findImageData.length; i++) {
              var imageConfig = imageResize.resize.find(e => e.id == ((req.query.reSizeId)  || 1));
              var params = {
                  MessageAttributes: {
                  "variation": {
                       DataType: "String",
                       StringValue: findImageData[i].variation
                   },
                   "width": {
                      DataType: "Number",
                      StringValue: imageConfig.width.toString()
                  },
                  "height": {
                    DataType: "Number",
                    StringValue: imageConfig.height.toString()
                   },
                  "quality": {
                      DataType: "Number",
                      StringValue: imageConfig.quality.toString()
                  },
                  "folder": {
                    DataType: "String",
                    StringValue: imageConfig.folder
                },
                 //  "customer": {
                 //     DataType: "String",
                 //     StringValue: s3Env.customerBucketUrl
                 // },
                 // "db": {
                 //     DataType: "String",
                 //    StringValue: mongooseUrl.url
                 // } // subscribe customer
                  },      
                  Id: 'id'+parseInt(Math.random()*1000000),
                  MessageBody: JSON.stringify(findImageData[i])
                 };
                 arrayOfSqs.push(params) 
               } 
              for (var i=0; i<arrayOfSqs.length; i+=size) {
                arrayOfArrays.push(arrayOfSqs.slice(i,i+size));
               }
                
               var updateSQSsentMessagesArray = findImageData.map(e => e.productImage._id);
               arrayOfSqsBatch(updateSQSsentMessagesArray, arrayOfArrays, res);
            } else{
              res.status(200).send({
                "result": 'no image'
              });
            }
          }
        });
      }
      

      exports.mainCategoryImageCount = function (req, res) { 
        Product.aggregate( [  { $unwind : "$productImage" }, {
            $match: {
                $and: [{ mainCategoryId: ObjectId(req.params.mainid)}, { "productImage.updateUrl": { $exists: false }}]
            }
          },  {
            $project: {
              superCategoryId: 1,
              mainCategoryId:1,
              subCategoryId:1,
              _id: 1,
              productImage: 1
            }
          },
        { '$count'    : "total" }
      ], function(err, findCountData) {
          if(err){
            res.status(500).send({
              "result": 'error occured while retreiving data'
            });
          } else {
             if(findCountData.length > 0) {
              var total = {
                total: findCountData[0].total
              };
              res.status(200).send(total);
            } else {
              var total = {
                total: 0
              };
              res.status(200).send(total);
            }
          }
        });
      }
exports.mainCategoryImageUrlToSqs = function (req, res) {
  var page = parseInt(req.query.page) || 0; //for next page pass 1 here
    var limit = parseInt(req.query.limit) || 100
    var skip = page * limit;
      Product.aggregate( [ 
          { $unwind : "$productImage" },  {
              $match: {
                  $and: [{ mainCategoryId: ObjectId(req.params.mainid)}, { "productImage.updateUrl": { $exists: false }}]
              }
          },  {
            $project: {
              superCategoryId: 1,
              mainCategoryId:1,
              subCategoryId:1,
              variation: 1,
              _id: 1,
              productImage: 1
            }
          },
          {$skip: skip },
          {$limit: limit }
    ], function(err, findImageData) {
        if(err) {
          res.status(500).send({
            "result": 'error occured while retreiving data'
          });
        } else {
          if(findImageData.length > 0) {
            var size = 10; var arrayOfArrays = []; var arrayOfSqs = [];
            for(i = 0; i < findImageData.length; i++) {
            var imageConfig = imageResize.resize.find(e => e.id == ((req.query.reSizeId)  || 1));
            var params = {
                MessageAttributes: {
                "variation": {
                     DataType: "String",
                     StringValue: findImageData[i].variation
                 },
                 "width": {
                    DataType: "Number",
                    StringValue: imageConfig.width.toString()
                },
                "height": {
                  DataType: "Number",
                  StringValue: imageConfig.height.toString()
                 },
                "quality": {
                    DataType: "Number",
                    StringValue: imageConfig.quality.toString()
                },
                "folder": {
                  DataType: "String",
                  StringValue: imageConfig.folder
              },
                /* "customer": {
                    DataType: "String",
                    StringValue: s3Env.customerBucketUrl
                },
                "db": {
                    DataType: "String",
                    StringValue: mongooseUrl.url
                } */ // subscribe customer
                },      
                Id: 'id'+parseInt(Math.random()*1000000),
                MessageBody: JSON.stringify(findImageData[i])
               };
               arrayOfSqs.push(params) 
            } 
            for (var i=0; i<arrayOfSqs.length; i+=size) {
              arrayOfArrays.push(arrayOfSqs.slice(i,i+size));
             } 
            var updateSQSsentMessagesArray = findImageData.map(e => e.productImage._id);
            arrayOfSqsBatch(updateSQSsentMessagesArray, arrayOfArrays, res); 
          } else{
            res.status(200).send({
              "result": 'no image'
            });
          }
        }
      });
    }
      
    
    exports.subCategoryImageCount = function (req, res) { 
      Product.aggregate( [  { $unwind : "$productImage" }, {
          $match: {
              $and: [{subCategoryId: ObjectId(req.params.subid)}, { "productImage.updateUrl": { $exists: false }}]
          }
        },  {
          $project: {
            superCategoryId: 1,
            mainCategoryId:1,
            subCategoryId: 1,
            _id: 1,
            productImage: 1
          }
        },
      { '$count'    : "total" }
    ], function(err, findCountData) {
        if(err){
          res.status(500).send({
            "result": 'error occured while retreiving data'
          });
        } else {
           if(findCountData.length > 0) {
            var total = {
              total: findCountData[0].total
            };
            res.status(200).send(total);
          } else {
            var total = {
              total: 0
            };
            res.status(200).send(total);
          }
        }
      });
    }
exports.subCategoryImageUrlToSqs = function (req, res) {
var page = parseInt(req.query.page) || 0; //for next page pass 1 here
  var limit = parseInt(req.query.limit) || 100
  var skip = page * limit;
    Product.aggregate( [ 
        { $unwind : "$productImage" },  {
            $match: {
                $and: [{ subCategoryId: ObjectId(req.params.subid)}, { "productImage.updateUrl": { $exists: false }} ]
            }
        },  {
          $project: {
            superCategoryId: 1,
            mainCategoryId:1,
            subCategoryId:1,
            variation: 1,
            _id: 1,
            productImage: 1
          }
        },
        {$skip: skip },
        {$limit: limit }
  ], function(err, findImageData) {
      if(err) {
        res.status(500).send({
          "result": 'error occured while retreiving data'
        });
      } else {
        
        if(findImageData.length > 0) {
          var size = 10; var arrayOfArrays = []; var arrayOfSqs = [];
          for(i = 0; i < findImageData.length; i++) {
          var imageConfig = imageResize.resize.find(e => e.id == ((req.query.reSizeId)  || 1));
          var params = {
              MessageAttributes: {
              "variation": {
                   DataType: "String",
                   StringValue: findImageData[i].variation
               },
               "width": {
                  DataType: "Number",
                  StringValue: imageConfig.width.toString()
              },
              "height": {
                DataType: "Number",
                StringValue: imageConfig.height.toString()
               },
              "quality": {
                  DataType: "Number",
                  StringValue: imageConfig.quality.toString()
              },
              "folder": {
                DataType: "String",
                StringValue: imageConfig.folder
            },
             /*  "customer": {
                  DataType: "String",
                  StringValue: s3Env.customerBucketUrl
              },
              "db": {
                  DataType: "String",
                  StringValue: mongooseUrl.url
              } */ // subscribe customer
              },      
              Id: 'id'+parseInt(Math.random()*1000000),
              MessageBody: JSON.stringify(findImageData[i])
             };
             arrayOfSqs.push(params) 
          } 
          for (var i=0; i<arrayOfSqs.length; i+=size) {
            arrayOfArrays.push(arrayOfSqs.slice(i,i+size));
           }
           var updateSQSsentMessagesArray = findImageData.map(e => e.productImage._id); 
          arrayOfSqsBatch(updateSQSsentMessagesArray, arrayOfArrays, res); 
        } else{
          res.status(200).send({
            "result": 'no image'
          });
        }
      }
    });
  }
  exports.productImagesUrlToSqs = function (req, res) {
    var page = parseInt(req.query.page) || 0; //for next page pass 1 here
      var limit = parseInt(req.query.limit) || 100
      var skip = page * limit;
        Product.aggregate( [ 
            { $unwind : "$productImage" },  {
                $match: {
                    $and: [{ _id: ObjectId(req.params.productid)},  { "productImage.updateUrl": { $exists: false }}]
                }
            },  {
              $project: {
                superCategoryId: 1,
                mainCategoryId:1,
                subCategoryId:1,
                variation: 1,
                _id: 1,
                productImage: 1
              }
            },
            {$skip: skip },
            {$limit: limit }
      ], function(err, findImageData) {
          if(err) {
            res.status(500).send({
              "result": 'error occured while retreiving data'
            });
          } else {
            if(findImageData.length > 0) {
              var imageConfig = imageResize.resize.find(e => e.id == ((req.query.reSizeId)  || 1));
              var size = 10; var arrayOfArrays = []; var arrayOfSqs = [];
              for(i = 0; i < findImageData.length; i++) {
              var params = {
                  MessageAttributes: {
                  "variation": {
                       DataType: "String",
                       StringValue: findImageData[i].variation
                   },
                   "width": {
                      DataType: "Number",
                      StringValue: imageConfig.width.toString()
                  },
                  "height": {
                    DataType: "Number",
                    StringValue: imageConfig.height.toString()
                   },
                  "quality": {
                      DataType: "Number",
                      StringValue: imageConfig.quality.toString()
                  },
                  "folder": {
                    DataType: "String",
                    StringValue: imageConfig.folder
                },
                 /*  "customer": {
                      DataType: "String",
                      StringValue: s3Env.customerBucketUrl
                  },
                  "db": {
                      DataType: "String",
                      StringValue: mongooseUrl.url
                  } */ // subscribe customer
                  },      
                  Id: 'id'+parseInt(Math.random()*1000000),
                  MessageBody: JSON.stringify(findImageData[i])
                 };
                 arrayOfSqs.push(params) 
              }
              
              console.log(imageConfig, 'image config')
              for (var i=0; i<arrayOfSqs.length; i+=size) {
                arrayOfArrays.push(arrayOfSqs.slice(i,i+size));
               } 
               var updateSQSsentMessagesArray = findImageData.map(e => e.productImage._id); 
              arrayOfSqsBatch(updateSQSsentMessagesArray, arrayOfArrays, res); 
            } else{
              res.status(200).send({
                "result": 'no image'
              });
            }
          }
        });
      }    
    async function arrayOfSqsBatch(updateSQSsentMessagesArray, arrayOfArrays, res)  {
      // Flood SQS Queue
      for (let i=0; i< arrayOfArrays.length; i++) {
         var params = { QueueUrl: sqsEnv.queueUrl, Entries: arrayOfArrays[i]};
          await sqs.sendMessageBatch(params).promise();
        }
        var bulk = Product.collection.initializeOrderedBulkOp();
               updateSQSsentMessagesArray.forEach(el => {
                bulk.find({ "productImage._id": el}).update({ "$set": { "productImage.$.updateUrl": false }});
              });
        bulk.execute(function (err, data) {
          if(err){
            res.status(500).send(err);
          } else {
            res.status(200).send(data);
          }
  });
}
  
      