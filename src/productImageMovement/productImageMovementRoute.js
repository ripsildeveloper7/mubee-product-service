'use strict';

var parentImageMovementMgr = require('./parentImageMovement/parentImageMovementMgr');
var childImageMovementMgr = require('./childImageMovement/childImageMovementMgr');
var categoryMgr = require('./categoryImages/categoryMgr');



module.exports = function (app) {
    // parent
    app.route('/supercategoryimagescount/supercategory/:superid')
    .get(parentImageMovementMgr.superCategoryImageCount);
    app.route('/supercategoryimages/supercategory/:superid')
    .get(parentImageMovementMgr.superCategoryImageUrlToSqs);
    app.route('/maincategoryimagescount/maincategory/:mainid')
    .get(parentImageMovementMgr.mainCategoryImageCount);
    app.route('/maincategoryimages/maincategory/:mainid')
    .get(parentImageMovementMgr.mainCategoryImageUrlToSqs);
    app.route('/subcategoryimagescount/subcategory/:subid')
    .get(parentImageMovementMgr.subCategoryImageCount);
    app.route('/subcategoryimages/subcategory/:subid')
    .get(parentImageMovementMgr.subCategoryImageUrlToSqs);
    app.route('/productimages/product/:productid')
    .get(parentImageMovementMgr.productImagesUrlToSqs);
    // child
    app.route('/supercategorychildimagescount/supercategory/:superid')
    .get(childImageMovementMgr.superCategoryChildImagesCount);
    app.route('/supercategorychildimages/supercategory/:superid')
    .get(childImageMovementMgr.superCategoryChildImagesUrlToSqs);
    app.route('/maincategorychildimagescount/maincategory/:mainid')
    .get(childImageMovementMgr.mainCategoryChildImagesCount);
    app.route('/maincategorychildimages/maincategory/:mainid')
    .get(childImageMovementMgr.mainCategoryChildImagesUrlToSqs);
    app.route('/subcategorychildimagescount/subcategory/:subid')
    .get(childImageMovementMgr.subCategoryChildImagesCount);
    app.route('/subcategorychildimages/subcategory/:subid')
    .get(childImageMovementMgr.subCategoryChildImagesUrlToSqs);
    app.route('/productchildimages/product/:productid')
    .get(childImageMovementMgr.productChildImagesUrlToSqs);
    app.route('/viewimagesupercategory')
    .get(categoryMgr.superCategoryView);
}
