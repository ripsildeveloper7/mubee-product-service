var SuperCategory = require('../../model/superCategory.model');



exports.superCategoryView = function (req, res) { 
    SuperCategory.aggregate( [   {
            $project: {
                _id: '$_id',
                categoryName: '$categoryName',
                mainCategory: '$mainCategory',
            }
          }
    ], function(err, findImageData) {
        if(err) {
          res.status(500).send({
            "result": 'error occured while retreiving data'
          });
        } else {
            res.status(200).send(findImageData);
        }
      });
  }

