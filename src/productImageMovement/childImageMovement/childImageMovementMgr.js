var childImageMovementDA = require('./childImageMovementDA');

exports.superCategoryChildImagesCount = function (req, res) {
    try {
        childImageMovementDA.superCategoryChildImagesCount(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  exports.superCategoryChildImagesUrlToSqs = function (req, res) {
    try {
        childImageMovementDA.superCategoryChildImagesUrlToSqs(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  exports.mainCategoryChildImagesCount = function (req, res) {
    try {
        childImageMovementDA.mainCategoryChildImagesCount(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  exports.mainCategoryChildImagesUrlToSqs = function (req, res) {
    try {
        childImageMovementDA.mainCategoryChildImagesUrlToSqs(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  
  exports.subCategoryChildImagesCount = function (req, res) {
    try {
        childImageMovementDA.subCategoryChildImagesCount(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  exports.subCategoryChildImagesUrlToSqs = function (req, res) {
    try {
        childImageMovementDA.subCategoryChildImagesUrlToSqs(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  exports.productChildImagesUrlToSqs = function (req, res) {
    try {
        childImageMovementDA.productChildImagesUrlToSqs(req, res);
    } catch (error) {
      console.log(error);
    }
  }