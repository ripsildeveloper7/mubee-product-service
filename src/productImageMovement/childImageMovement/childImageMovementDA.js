var Product = require('../../model/product.model');
var imageResize = require('./../../config/imageResize');
var s3Env = require('../../config/s3.env');
var sqsEnv = require('../../config/sqs.env');
var mongooseUrl = require('../../config/mongoDatabase.config');
const sqs = require('../../config/sqs.config');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
exports.superCategoryChildImagesCount = function (req, res) {
    Product.aggregate([{
        $project: {
            superCategoryId: 1,
            _id: 1,
            "variationType": 1,
            "child._id": 1,
            "child.headChild": 1,
            "child.productImage": 1,
            "child.colorId": 1,
            "child.sizeVariantId": 1,
            "child.sizeVariant": 1,
            "child.variation": 1
        }
    },
    {
        $unwind: "$child"
    }, {
        $unwind: "$child.productImage"
    }, {
        $match: {
            $and: [{ superCategoryId: ObjectId(req.params.superid) },
            { "child.productImage.updateUrl": { $exists: false } }]
        }
    }, { '$count': "total" }], function (err, findCountData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            });
        } else {
            if (findCountData.length > 0) {
                var total = {
                    total: findCountData[0].total
                };
                res.status(200).send(total);
            } else {
                var total = {
                    total: 0
                };
                res.status(200).send(total);
            }
        }
    });
}


exports.superCategoryChildImagesUrlToSqs = function (req, res) {
    var page = parseInt(req.query.page) || 0; //for next page pass 1 here
    var limit = parseInt(req.query.limit) || 100
    var skip = page * limit;
    Product.aggregate([{
        $project: {
            superCategoryId: 1,
            _id: 1,
            "variation": 1,
            "variationType": 1,
            "child._id": 1,
            "child.headChild": 1,
            "child.productImage": 1,
            "child.colorId": 1,
            "child.variation": 1
        }
    },
    {
        $unwind: "$child"
    }, {
        $unwind: "$child.productImage"
    },
    {
        $match: {
            $and: [{ superCategoryId: ObjectId(req.params.superid) },
            { "child.productImage.updateUrl": { $exists: false } }]
        }
    },
    { $skip: skip },
    { $limit: limit }
    ], function (err, findImageData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            });
        } else {
            var productNoneAndSizeVariant = findImageData.filter(e => (e.variationType === 'None' || e.variationType === 'Size') && e.child.headChild === true);
            var productColorVariant = findImageData.filter(e => e.variationType === 'Color');
            var productSizeColorVariant = findImageData.filter(e => e.variationType === 'SizeColor');
            var arrResult = [];
            var nonDuplicatedArray = [];
            for (let i = productSizeColorVariant.length - 1; i >= 0; i--) {
                var item = productSizeColorVariant[i];
                arrResult[item._id + '-' + item.child.colorId] = item; // create associative array
            }
            var i = 0;
            for (var item in arrResult) {
                nonDuplicatedArray[i++] = arrResult[item]; // copy the objects that are now unique
            }
            var realSizeAndColorVariant = [];
            for (var item in nonDuplicatedArray) {
                var sizeColor = productSizeColorVariant.filter(e => e.child._id.toString() == nonDuplicatedArray[item].child._id);
                sizeColor.forEach(el => {
                    realSizeAndColorVariant.push(el);
                })
            }
            var findImageDataAll = [...productNoneAndSizeVariant, ...productColorVariant, ...realSizeAndColorVariant];
            var imageConfig = imageResize.resize.find(e => e.id == ((req.query.reSizeId) || 2));
            if (findImageDataAll.length > 0) {
                var size = 10; var arrayOfArrays = []; var arrayOfSqs = [];
                for (i = 0; i < findImageDataAll.length; i++) {

                    var params = {
                        MessageAttributes: {
                            "variation": {
                                DataType: "String",
                                StringValue: findImageDataAll[i].child.variation
                            },
                            "variationType": {
                                DataType: "String",
                                StringValue: findImageDataAll[i].variationType
                            },
                            "width": {
                                DataType: "Number",
                                StringValue: imageConfig.width.toString()
                            },
                            "height": {
                                DataType: "Number",
                                StringValue: imageConfig.height.toString()
                            },
                            "quality": {
                                DataType: "Number",
                                StringValue: imageConfig.quality.toString()
                            },
                            "folder": {
                                DataType: "String",
                                StringValue: imageConfig.folder
                            },
                            /*  "customer": {
                                 DataType: "String",
                                 StringValue: s3Env.catalogueBucketName
                             },
                             "db": {
                                 DataType: "String",
                                 StringValue: mongooseUrl.url
                             } */ // subscribe customer
                        },
                        Id: 'id' + parseInt(Math.random() * 1000000),
                        MessageBody: JSON.stringify(findImageDataAll[i])
                    };
                    arrayOfSqs.push(params)
                }
                for (var i = 0; i < arrayOfSqs.length; i += size) {
                    arrayOfArrays.push(arrayOfSqs.slice(i, i + size));
                }
                var arr = findImageData.map(function (item, index, array) {
                    // returns the new value instead of item
                    return item.child._id.toString();
                });
                var updateSQSsentMessagesArray = arr.filter(function (item, index, inputArray) {
                    return inputArray.indexOf(item) == index;
                });
                arrayOfSqsBatch(updateSQSsentMessagesArray, arrayOfArrays, res);
            } else {
                res.status(200).send({
                    "result": 'no image'
                });
            }
        }
    })
}


exports.mainCategoryChildImagesCount = function (req, res) {
    Product.aggregate([{
        $project: {
            mainCategoryId: 1,
            _id: 1,
            "variationType": 1,
            "child._id": 1,
            "child.headChild": 1,
            "child.productImage": 1,
            "child.colorId": 1,
            "child.sizeVariantId": 1,
            "child.sizeVariant": 1,
            "child.variation": 1
        }
    },
    {
        $unwind: "$child"
    }, {
        $unwind: "$child.productImage"
    }, {
        $match: {
            $and: [{ mainCategoryId: ObjectId(req.params.mainid) },
            { "child.productImage.updateUrl": { $exists: false } }]
        }
    }, { '$count': "total" }], function (err, findCountData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            });
        } else {
            if (findCountData.length > 0) {
                var total = {
                    total: findCountData[0].total
                };
                res.status(200).send(total);
            } else {
                var total = {
                    total: 0
                };
                res.status(200).send(total);
            }
        }
    });
}


exports.mainCategoryChildImagesUrlToSqs = function (req, res) {
    var page = parseInt(req.query.page) || 0; //for next page pass 1 here
    var limit = parseInt(req.query.limit) || 100
    var skip = page * limit;
    Product.aggregate([{
        $project: {

            mainCategoryId: 1,
            _id: 1,
            "variation": 1,
            "variationType": 1,
            "child._id": 1,
            "child.headChild": 1,
            "child.productImage": 1,
            "child.colorId": 1,
            "child.sizeVariantId": 1,
            "child.sizeVariant": 1,
            "child.variation": 1
        }
    },
    {
        $unwind: "$child"
    }, {
        $unwind: "$child.productImage"
    },
    {
        $match: {
            $and: [{ mainCategoryId: ObjectId(req.params.mainid) }, { "child.productImage.updateUrl": false }]
        }
    },
    { $skip: skip },
    { $limit: limit }
    ], function (err, findImageData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            });
        } else {
            var productNoneAndSizeVariant = findImageData.filter(e => (e.variationType === 'None' || e.variationType === 'Size') && e.child.headChild === true);
            var productColorVariant = findImageData.filter(e => e.variationType === 'Color');
            var productSizeColorVariant = findImageData.filter(e => e.variationType === 'SizeColor');
            var arrResult = [];
            var nonDuplicatedArray = [];
            for (let i = productSizeColorVariant.length - 1; i >= 0; i--) {
                var item = productSizeColorVariant[i];
                arrResult[item._id + '-' + item.child.colorId] = item; // create associative array
            }
            var i = 0;
            for (var item in arrResult) {
                nonDuplicatedArray[i++] = arrResult[item]; // copy the objects that are now unique
            }
            var realSizeAndColorVariant = [];
            for (var item in nonDuplicatedArray) {
                var sizeColor = productSizeColorVariant.filter(e => e.child._id.toString() == nonDuplicatedArray[item].child._id);
                sizeColor.forEach(el => {
                    realSizeAndColorVariant.push(el);
                })
            }
            var findImageDataAll = [...productNoneAndSizeVariant, ...productColorVariant, ...realSizeAndColorVariant];

            if (findImageDataAll.length > 0) {
                var imageConfig = imageResize.resize.find(e => e.id == ((req.query.reSizeId) || 2));
                var size = 10; var arrayOfArrays = []; var arrayOfSqs = [];
                for (i = 0; i < findImageDataAll.length; i++) {

                    var params = {
                        MessageAttributes: {
                            "variation": {
                                DataType: "String",
                                StringValue: findImageDataAll[i].child.variation
                            },
                            "variationType": {
                                DataType: "String",
                                StringValue: findImageDataAll[i].variationType
                            },
                            "width": {
                                DataType: "Number",
                                StringValue: imageConfig.width.toString()
                            },
                            "height": {
                                DataType: "Number",
                                StringValue: imageConfig.height.toString()
                            },
                            "quality": {
                                DataType: "Number",
                                StringValue: imageConfig.quality.toString()
                            },
                            "folder": {
                                DataType: "String",
                                StringValue: imageConfig.folder
                            },
                            /*  "customer": {
                                 DataType: "String",
                                 StringValue: s3Env.catalogueBucketName
                             },
                             "db": {
                                 DataType: "String",
                                 StringValue: mongooseUrl.url
                             } */ // subscribe customer
                        },
                        Id: 'id' + parseInt(Math.random() * 1000000),
                        MessageBody: JSON.stringify(findImageDataAll[i])
                    };
                    arrayOfSqs.push(params)
                }
                for (var i = 0; i < arrayOfSqs.length; i += size) {
                    arrayOfArrays.push(arrayOfSqs.slice(i, i + size));
                }
                var arr = findImageData.map(function (item, index, array) {
                    // returns the new value instead of item
                    return item.child._id.toString();
                });
                var updateSQSsentMessagesArray = arr.filter(function (item, index, inputArray) {
                    return inputArray.indexOf(item) == index;
                });
                arrayOfSqsBatch(updateSQSsentMessagesArray, arrayOfArrays, res);
            } else {
                res.status(200).send({
                    "result": 'no image'
                });
            }
        }
    })
}

// subcategroy child 
exports.subCategoryChildImagesCount = function (req, res) {
    Product.aggregate([{
        $project: {
            subCategoryId: 1,
            _id: 1,
            "variationType": 1,
            "child._id": 1,
            "child.headChild": 1,
            "child.productImage": 1,
            "child.colorId": 1,
            "child.sizeVariantId": 1,
            "child.sizeVariant": 1,
            "child.variation": 1
        }
    },
    {
        $unwind: "$child"
    }, {
        $unwind: "$child.productImage"
    }, {
        $match: {
            $and: [{ subCategoryId: ObjectId(req.params.subid) },
            { "child.productImage.updateUrl": { $exists: false } }]
        }
    }, { '$count': "total" }], function (err, findCountData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            });
        } else {
            if (findCountData.length > 0) {
                var total = {
                    total: findCountData[0].total
                };
                res.status(200).send(total);
            } else {
                var total = {
                    total: 0
                };
                res.status(200).send(total);
            }
        }
    });
}

// subcategory child sqs

exports.subCategoryChildImagesUrlToSqs = function (req, res) {
    var page = parseInt(req.query.page) || 0; //for next page pass 1 here
    var limit = parseInt(req.query.limit) || 100
    var skip = page * limit;
    Product.aggregate([{
        $project: {
            subCategoryId: 1,
            _id: 1,
            "variation": 1,
            "variationType": 1,
            "child._id": 1,
            "child.headChild": 1,
            "child.productImage": 1,
            "child.colorId": 1,
            "child.sizeVariantId": 1,
            "child.sizeVariant": 1,
            "child.variation": 1
        }
    },
    {
        $unwind: "$child"
    }, {
        $unwind: "$child.productImage"
    },
    {
        $match: {
            $and: [{ subCategoryId: ObjectId(req.params.subid) },
            { "child.productImage.updateUrl": { $exists: false } }]
        }
    },
    { $skip: skip },
    { $limit: limit }
    ], function (err, findImageData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            });
        } else {
            var productNoneAndSizeVariant = findImageData.filter(e => (e.variationType === 'None' || e.variationType === 'Size') && e.child.headChild === true);
            var productColorVariant = findImageData.filter(e => e.variationType === 'Color');
            var productSizeColorVariant = findImageData.filter(e => e.variationType === 'SizeColor');
            var arrResult = [];
            var nonDuplicatedArray = [];
            for (let i = productSizeColorVariant.length - 1; i >= 0; i--) {
                var item = productSizeColorVariant[i];
                arrResult[item._id + '-' + item.child.colorId] = item; // create associative array
            }
            var i = 0;
            for (var item in arrResult) {
                nonDuplicatedArray[i++] = arrResult[item]; // copy the objects that are now unique
            }
            var realSizeAndColorVariant = [];
            for (var item in nonDuplicatedArray) {
                var sizeColor = productSizeColorVariant.filter(e => e.child._id.toString() == nonDuplicatedArray[item].child._id);
                sizeColor.forEach(el => {
                    realSizeAndColorVariant.push(el);
                })
            }
            var findImageDataAll = [...productNoneAndSizeVariant, ...productColorVariant, ...realSizeAndColorVariant];
            var imageConfig = imageResize.resize.find(e => e.id == ((req.query.reSizeId) || 2));
            if (findImageDataAll.length > 0) {
                var size = 10; var arrayOfArrays = []; var arrayOfSqs = [];
                for (i = 0; i < findImageDataAll.length; i++) {

                    var params = {
                        MessageAttributes: {
                            "variation": {
                                DataType: "String",
                                StringValue: findImageDataAll[i].child.variation
                            },
                            "variationType": {
                                DataType: "String",
                                StringValue: findImageDataAll[i].variationType
                            },
                            "width": {
                                DataType: "Number",
                                StringValue: imageConfig.width.toString()
                            },
                            "height": {
                                DataType: "Number",
                                StringValue: imageConfig.height.toString()
                            },
                            "quality": {
                                DataType: "Number",
                                StringValue: imageConfig.quality.toString()
                            },
                            "folder": {
                                DataType: "String",
                                StringValue: imageConfig.folder
                            },
                            /*  "customer": {
                                 DataType: "String",
                                 StringValue: s3Env.catalogueBucketName
                             },
                             "db": {
                                 DataType: "String",
                                 StringValue: mongooseUrl.url
                             } */ // subscribe customer
                        },
                        Id: 'id' + parseInt(Math.random() * 1000000),
                        MessageBody: JSON.stringify(findImageDataAll[i])
                    };
                    arrayOfSqs.push(params)
                }
                for (var i = 0; i < arrayOfSqs.length; i += size) {
                    arrayOfArrays.push(arrayOfSqs.slice(i, i + size));
                }
                var arr = findImageData.map(function (item, index, array) {
                    // returns the new value instead of item
                    return item.child._id.toString();
                });
                var updateSQSsentMessagesArray = arr.filter(function (item, index, inputArray) {
                    return inputArray.indexOf(item) == index;
                });
                arrayOfSqsBatch(updateSQSsentMessagesArray, arrayOfArrays, res);
            } else {
                res.status(200).send({
                    "result": 'no image'
                });
            }
        }
    })
}

// product child sqs
exports.productChildImagesUrlToSqs = function (req, res) {
    var page = parseInt(req.query.page) || 0; //for next page pass 1 here
    var limit = parseInt(req.query.limit) || 100
    var skip = page * limit;
    Product.aggregate([{
        $project: {
            _id: 1,
            "variation": 1,
            "variationType": 1,
            "child._id": 1,
            "child.headChild": 1,
            "child.productImage": 1,
            "child.colorId": 1,
            "child.sizeVariantId": 1,
            "child.sizeVariant": 1,
            "child.variation": 1
        }
    },
    {
        $unwind: "$child"
    }, {
        $unwind: "$child.productImage"
    },
    {
        $match: {
            $and: [{ _id: ObjectId(req.params.productid) }, { "child.productImage.updateUrl": { $exists: false } }]
        }
    },
    { $skip: skip },
    { $limit: limit }
    ], function (err, findImageData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            });
        } else {
            var productNoneAndSizeVariant = findImageData.filter(e => (e.variationType === 'None' || e.variationType === 'Size') && e.child.headChild === true);
            var productColorVariant = findImageData.filter(e => e.variationType === 'Color');
            var productSizeColorVariant = findImageData.filter(e => e.variationType === 'SizeColor');
            var arrResult = [];
            var nonDuplicatedArray = [];
            for (let i = productSizeColorVariant.length - 1; i >= 0; i--) {
                var item = productSizeColorVariant[i];
                arrResult[item._id + '-' + item.child.colorId] = item; // create associative array
            }
            var i = 0;
            for (var item in arrResult) {
                nonDuplicatedArray[i++] = arrResult[item]; // copy the objects that are now unique
            }
            var realSizeAndColorVariant = [];
            for (var item in nonDuplicatedArray) {
                var sizeColor = productSizeColorVariant.filter(e => e.child._id.toString() == nonDuplicatedArray[item].child._id);
                sizeColor.forEach(el => {
                    realSizeAndColorVariant.push(el);
                })
            }
            var findImageDataAll = [...productNoneAndSizeVariant, ...productColorVariant, ...realSizeAndColorVariant];
            if (findImageDataAll.length > 0) {
                var imageConfig = imageResize.resize.find(e => e.id == ((req.query.reSizeId) || 2));
                var size = 10; var arrayOfArrays = []; var arrayOfSqs = [];
                for (i = 0; i < findImageDataAll.length; i++) {
                    var params = {
                        MessageAttributes: {
                            "variation": {
                                DataType: "String",
                                StringValue: findImageDataAll[i].child.variation
                            },
                            "variationType": {
                                DataType: "String",
                                StringValue: findImageDataAll[i].variationType
                            },
                            "width": {
                                DataType: "Number",
                                StringValue: imageConfig.width.toString()
                            },
                            "height": {
                                DataType: "Number",
                                StringValue: imageConfig.height.toString()
                            },
                            "quality": {
                                DataType: "Number",
                                StringValue: imageConfig.quality.toString()
                            },
                            "folder": {
                                DataType: "String",
                                StringValue: imageConfig.folder
                            },
                            /*  "customer": {
                                 DataType: "String",
                                 StringValue: s3Env.catalogueBucketName
                             },
                             "db": {
                                 DataType: "String",
                                 StringValue: mongooseUrl.url
                             } */ // subscribe customer
                        },
                        Id: 'id' + parseInt(Math.random() * 1000000),
                        MessageBody: JSON.stringify(findImageDataAll[i])
                    };
                    arrayOfSqs.push(params)
                }
                for (var i = 0; i < arrayOfSqs.length; i += size) {
                    arrayOfArrays.push(arrayOfSqs.slice(i, i + size));
                }
                var arr = findImageData.map(function (item, index, array) {
                    // returns the new value instead of item
                    return item.child._id.toString();
                });
                var updateSQSsentMessagesArray = arr.filter(function (item, index, inputArray) {
                    return inputArray.indexOf(item) == index;
                });
                arrayOfSqsBatch(updateSQSsentMessagesArray, arrayOfArrays, res);
            } else {
                res.status(200).send({
                    "result": 'no image'
                });
            }
        }
    })
}
async function arrayOfSqsBatch(updateSQSsentMessagesArray, arrayOfArrays, res) {
    // Flood SQS Queue
    for (let i = 0; i < arrayOfArrays.length; i++) {
        var params = { QueueUrl: sqsEnv.queueUrl, Entries: arrayOfArrays[i] };
        await sqs.sendMessageBatch(params).promise()
    }
    var bulk = Product.collection.initializeOrderedBulkOp();
    updateSQSsentMessagesArray.forEach(et => {
        // var setModifier = { $set: {} };
        // setModifier.$set["child.$[].productImage.$[field].updateUrl"] = false;

        var setModifier = { $set: {} };
        setModifier.$set["child.$.productImage.$[].updateUrl"] = false;
        bulk.find({ "child._id": ObjectId(et) }).update(setModifier);
    });
    bulk.execute(function (err, data) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).send(data);
        }
    });
}

