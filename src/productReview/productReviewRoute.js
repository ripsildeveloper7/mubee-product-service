var reviewMgr = require('./productReviewMgr');

module.exports = function (app) {
    app.route('/createproductreview')
    .post(reviewMgr.createReview); // create product review

    app.route('/getselectedproductreview/:id')
    .get(reviewMgr.getSelectedProductReview); // get selected product review

    app.route('/getallreviewwithproduct')
    .get(reviewMgr.getProductReviewWithProduct); // get All Review with Product

    app.route('/getsinglereviewwithproduct/:id')
    .get(reviewMgr.getSingleProductReviewWithProduct); // get Single Review with Product

    app.route('/deleteproductreview/:id')        // delete product review
    .delete(reviewMgr.deleteProductReview); 

    app.route('/updatepublishproductreview')    // update published product review
    .put(reviewMgr.updatePublishProductReview);

    app.route('/updateunpublishproductreview')      // update  unpublished product review
    .put(reviewMgr.UpdateUnPublishProductReview);

    app.route('/getpublishedreview')      // get  upublished Review 
    .get(reviewMgr.getPublishedReview);

    app.route('/getreviewforverification')      // get  upublished Review 
    .get(reviewMgr.getReviewForVerifyOrder);
}