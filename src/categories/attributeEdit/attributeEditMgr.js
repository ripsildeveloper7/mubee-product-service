var attributeEditDA = require('./attributeEditDA')


exports.SupCategoryAttributeFieldValue = function (req, res) {
    try {
        attributeEditDA.SupCategoryAttributeFieldValue(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.SupCategoryAttribute = function (req, res) {
    try {
        attributeEditDA.SupCategoryAttribute(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.SupCategoryAttributeDelete = function (req, res) {
    try {
        attributeEditDA.SupCategoryAttributeDelete(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.SupCategoryAttributeFieldValueDelete = function (req, res) {
    try {
        attributeEditDA.SupCategoryAttributeFieldValueDelete(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.SupCategoryAttributeAdd = function (req, res) {
    try {
        attributeEditDA.SupCategoryAttributeAdd(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.SupCategoryAttributeFieldValueAdd = function (req, res) {
    try {
        attributeEditDA.SupCategoryAttributeFieldValueAdd(req, res);
    } catch (error) {
        console.log(error);
    }
}


/* sub categor attribute */
exports.SubCategoryAttributeFieldValue = function (req, res) {
    try {
        attributeEditDA.SubCategoryAttributeFieldValue(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.SubCategoryAttribute = function (req, res) {
    try {
        attributeEditDA.SubCategoryAttribute(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.SubCategoryAttributeDelete = function (req, res) {
    try {
        attributeEditDA.SubCategoryAttributeDelete(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.SubCategoryAttributeFieldValueDelete = function (req, res) {
    try {
        attributeEditDA.SubCategoryAttributeFieldValueDelete(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.SubCategoryAttributeAdd = function (req, res) {
    try {
        attributeEditDA.SubCategoryAttributeAdd(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.SubCategoryAttributeFieldValueAdd = function (req, res) {
    try {
        attributeEditDA.SubCategoryAttributeFieldValueAdd(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.SubCategoryAttributeFieldValue = function (req, res) {
    try {
        attributeEditDA.SubCategoryAttributeFieldValue(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.SubCategoryAttribute = function (req, res) {
    try {
        attributeEditDA.SubCategoryAttribute(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.SubCategoryAttributeDelete = function (req, res) {
    try {
        attributeEditDA.SubCategoryAttributeDelete(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.SubCategoryAttributeFieldValueDelete = function (req, res) {
    try {
        attributeEditDA.SubCategoryAttributeFieldValueDelete(req, res);
    } catch (error) {
        console.log(error);
    }
}

 /* main Category Attribute */
 exports.MainCategoryAttributeFieldValue = function (req, res) {
    try {
        attributeEditDA.MainCategoryAttributeFieldValue(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.MainCategoryAttribute = function (req, res) {
    try {
        attributeEditDA.SubCategoryAttribute(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.MainCategoryAttributeDelete = function (req, res) {
    try {
        attributeEditDA.MainCategoryAttributeDelete(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.MainCategoryAttributeFieldValueDelete = function (req, res) {
    try {
        attributeEditDA.MainCategoryAttributeFieldValueDelete(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.MainCategoryAttributeAdd = function (req, res) {
    try {
        attributeEditDA.MainCategoryAttributeAdd(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.MainCategoryAttributeFieldValueAdd = function (req, res) {
    try {
        attributeEditDA.MainCategoryAttributeFieldValueAdd(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.MainCategoryAttributeFieldValue = function (req, res) {
    try {
        attributeEditDA.MainCategoryAttributeFieldValue(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.MainCategoryAttribute = function (req, res) {
    try {
        attributeEditDA.MainCategoryAttribute(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.MainCategoryAttributeDelete = function (req, res) {
    try {
        attributeEditDA.MainCategoryAttributeDelete(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.MainCategoryAttributeFieldValueDelete = function (req, res) {
    try {
        attributeEditDA.SubCategoryAttributeFieldValueDelete(req, res);
    } catch (error) {
        console.log(error);
    }
}