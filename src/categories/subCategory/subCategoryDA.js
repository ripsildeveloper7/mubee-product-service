var SuperCategory = require('../../model/superCategory.model');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

exports.subCategoryInsert = function (req, res) {

  SuperCategory.findById(req.params.superid, function (err, category) {


    var mainCat = category.mainCategory.id(req.params.mainid)

    mainCat.subCategory.push({
      subCategoryName: req.body.subCategoryName,
      subCategoryDescription: req.body.subCategoryDescription,
      metaTagTitle: req.body.metaTagTitle,
      metaTagDescription: req.body.metaTagDescription,
      metaTagKeyword: req.body.metaTagKeyword,
      attribute: req.body.attribute
    });

    category.save(function (err, result) {
      if (err) {
        res.status(500).send({
          "result": 1
        });
      } else {
        /* console.log(result); */
        for(let i=0; i <= result.mainCategory.length -1; i++) {
          if(result.mainCategory[i].id === req.params.mainid) {
            var temp1 = result.mainCategory[i].subCategory.length -1;
            var temp2 = result.mainCategory[i].subCategory[temp1];
          }
        }
        /* console.log(temp2); */
        res.status(200).json(temp2)
      }
    });

  });

}


exports.subCategoryDelete = function (req, res) {
  SuperCategory.findById(req.params.categoryId, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.mainCategoryId)
      mainCat.subCategory.id(req.params.subCategoryId).remove()
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          SuperCategory.findById(req.params.categoryId, function (err, category) {
            if (err) {
              res.status(500).send({
                "result": 0
              });
            } else {
              var mainCat = category.mainCategory.id(req.params.mainCategoryId)

              res.status(200).json(mainCat);
            }
          });
        }
      });
    }
  });


}

exports.subCategoryUpdate = function (req, res) {
  SuperCategory.findById(req.params.categoryId, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {

      var mainCat = category.mainCategory.id(req.params.mainCategoryId)
      var subCat = mainCat.subCategory.id(req.params.subCategoryId);
      subCat.subCategoryName = req.body.subCategoryName;
      subCat.subCategoryDescription = req.body.subCategoryDescription
      subCat.metaTagDescription = req.body.metaTagDescription,
      subCat.metaTagKeyword = req.body.metaTagKeyword
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          SuperCategory.findById(req.params.categoryId, function (err, category) {
            if (err) {
              res.status(500).send({
                "result": 0
              });
            } else {
              var mainCat = category.mainCategory.id(req.params.mainCategoryId)
              res.status(200).json(mainCat)
            }
          });
        }
      });
    }
  });
}




exports.findSubCategory = function (req, res) {
  SuperCategory.findById(req.params.categoryId, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.mainCategoryId)

      res.status(200).json(mainCat)
    }
  });
}

exports.findSubCategoryAttribute = function (req, res) {
  /* SuperCategory.find({'mainCategory.subCategory._id': req.params.subId}, {'mainCategory.subCategory.$': 1}).exec(function(err, subcategories) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
        console.log(subCatAttribute);
        var subCatAttribute = subcategories.filter(el => el.fieldType === 'Dropdown')
        res.status(200).json(subCatAttribute);
    }
  }); */
  SuperCategory.aggregate([{ $unwind: "$mainCategory" },
  { $unwind: "$mainCategory.subCategory" }, {$match: {'mainCategory.subCategory._id': ObjectId(req.params.subId)}} ]).exec(function(err, subcategories) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      if(subcategories[0]){
        var subCatAttribute = subcategories[0].mainCategory.subCategory.attribute.filter(el => el.fieldType === 'Dropdown')
        /* var subCatAttribute = .filter(el => el.fieldType === 'Dropdown') */
        res.status(200).json(subCatAttribute);
      } else {
        res.status(200).json(subCatAttribute);
      }
    }
  });
}
// webportal sub category
exports.findAllSubCategory = function (req, res) {
   /* SuperCategory.find({}).select().exec(function (err, subCat) {
       if (err) {
           res.status(500).send({
               "result": 0
           });
       } else {
           res.status(200).json(subCat);
       }
   }); */
  SuperCategory.aggregate([
    {$match: {_id: ObjectId(req.params.id)}},
    {
      $unwind: "$mainCategory"
    },
    {
      $project: {
        subCat: "$mainCategory.subCategory"
      }
    },
    {
      $unwind: "$subCat"
    }
  ], function (err, subCatData) {
    if (err) {
      res.status(500).send({
        message: "no category"
      });
    } else {
      var subAllData = subCatData.map(ele => ele.subCat)
      res.status(200).json(subAllData);
    }
  });
}


exports.addSubCategoryImage = function (req, file, res) {
  SuperCategory.findById(req.params.supId, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {

      var mainCat = category.mainCategory.id(req.params.mainId)
      var subCat = mainCat.subCategory.id(req.params.subId);
      subCat.subCategoryImageName = file.originalname;
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
         
          SuperCategory.find({}).select().exec(function (err, finddata) {
            if (err) {
              res.status(500).json(err);
            } else {
              res.status(200).json(finddata);
            }
          })

        }
      });
    }
  });
}
exports.findSingleSubCategory = function (req, res) {
  SuperCategory.findById(req.params.categoryId, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.mainCategoryId);
      var subCat = mainCat.subCategory.id(req.params.subCategoryId);
      res.status(200).json(subCat)
    }
  });
}