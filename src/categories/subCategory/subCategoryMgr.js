var subCategoryDA = require('./subCategoryDA')
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');

exports.subCategoryInsert = function (req, res) {
    try {
        subCategoryDA.subCategoryInsert(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.subCategoryDelete = function (req, res) {
    try {
        subCategoryDA.subCategoryDelete(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.subCategoryUpdate = function (req, res) {
    try {
        subCategoryDA.subCategoryUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}




exports.findSubCategory = function (req, res) {
    try {
        subCategoryDA.findSubCategory(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.findSubCategoryAttribute = function (req, res) {
    try {
        subCategoryDA.findSubCategoryAttribute(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.findAllSubCategory = function (req, res) {
    try {
        subCategoryDA.findAllSubCategory(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.addSubCategoryImage = function (req, res) {
    try {
        const file = req.files
        console.log(file);
        file.map((item) => {
            const params = {
              Bucket: env.Bucket + '/' + 'images' + '/' + 'subcategory'+ '/' + req.params.id,  // create a folder and save the image
              Key: item.originalname,
              Body: item.buffer,    
        };
        s3.upload(params, (err, data) => {
            if (err) {
              console.log(err);
            } else {
                subCategoryDA.addSubCategoryImage(req,item,res);
            }
        });
    }) 
    } catch (error) {
        console.log(error);
    }
}
exports.findSingleSubCategory = function (req, res) {
    try {
        subCategoryDA.findSingleSubCategory(req, res);
    } catch (error) {
        console.log(error);
    }
}