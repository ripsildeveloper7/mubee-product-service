'use strict';
var SuperCategory = require('../../model/superCategory.model');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;


exports.superCategoryInsert = function (req, res) {
  var superCategoryData = new SuperCategory(req.body);
  superCategoryData.categoryName = req.body.categoryName;
  superCategoryData.categoryDescription = req.body.categoryDescription;
  superCategoryData.sortOrder = req.body.sortOrder;
  superCategoryData.status = req.body.status;
  superCategoryData.isTailoringService = req.body.isTailoringService;
  superCategoryData.tailoringService = req.body.tailoringService;
  superCategoryData.attribute = req.body.attribute;
  superCategoryData.save(
    function (err, superCat) {
      if (err) { // if it contains error return 0
        res.status(500).send({
          "result": 0
        });
      } else {
        SuperCategory.find({}).select().exec(function (err, superCat) {
          if (err) {
            res.status(500).send({
              message: "Some error occurred while retrieving notes."
            });
          } else {
            res.status(200).json(superCat);
          }
        });
      }
    });
}

exports.superCategoryShow = function (req, res) {
  SuperCategory.find({}).select().sort({ sortOrder: 1 }).exec(function (err, superCat) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(superCat);
    }
  });
}

exports.superCategoryWithAttribute = function (req, res) {
  SuperCategory.find({}).select().sort({ sortOrder: 1 }).exec(function (err, superCat) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      console.log(superCat);
      var superCatAttribute = [];
      superCat.forEach(function (element) {
        element.attribute = element.attribute.filter((el) =>
          (el.fieldSetting !== 'Size') && (el.fieldType !== 'Text') && (el.fieldEnable === true)
        );
        superCatAttribute.push(element);
      });
      res.status(200).json(superCatAttribute);
    }
  });
}



exports.superCategoryDelete = function (req, res) {
  SuperCategory.findByIdAndRemove(req.params.categoryId, function (err) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      SuperCategory.find({}).select().exec(function (err, superCat) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(superCat);
        }
      });
    }
  });
}


exports.showSuperCategory = function (req, res) {
  SuperCategory.find({}).select().exec(function (err, superCat) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {

      res.status(200).json(superCat);
    }
  });
}

exports.superCategoryEdit = function (req, res) {
  SuperCategory.findById(req.params.id, function (err, superCat) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      superCat.categoryName = req.body.categoryName;
      superCat.categoryDescription = req.body.categoryDescription;
      superCat.sortOrder = req.body.sortOrder;
      superCat.status = req.body.status;
      superCat.isTailoringService = req.body.isTailoringService;
      superCat.tailoringService = req.body.tailoringService;
      /* res.status(200).json(superCat); */
      superCat.save(
        function (err, superCatupdate) {
          if (err) { // if it contains error return 0
            res.status(500).send({
              "result": 0
            });
          } else {
            SuperCategory.find({}).select().exec(function (err, superCat) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {

                res.status(200).json(superCat);
              }
            });
          }
        });
    }
  });
}


/* exports.showSuperCategoryByStatus = function (req, res) {
    SuperCategory.find({'status': 'Enabled'}).select().exec(function (err, superCat) {
      if (err) {
        res.status(500).send({
          message: "Some error occurred while retrieving notes."
        });
      } else {
        for(let i = 0; i <= superCat.length - 1; i++) {
            superCat[i].categoryImageName = env.ImageServerPath + 'catedory'+ '/' + superCat[i]._id + '/' + superCat[i].categoryImageName;
        }
        res.status(200).json(superCat);
      }
    });
  } */

exports.showSuperCategoryByStatus = function (req, res) {
  SuperCategory.aggregate([{
    $lookup: {
      from: 'products',
      localField: 'mainCategory.subCategory._id',
      foreignField: 'subCategoryId',
      as: 'categoryProduct'
    }
  }, {
    $sort: {
      sortOrder: 1
    }
  }
  ], function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  });
}

exports.supcat = function (req, res) {
  SuperCategory.aggregate([{
    $lookup: {
      from: 'products',
      localField: 'mainCategory.subCategory._id',
      foreignField: 'subCategoryId',
      as: 'categoryProduct'
    }
  }
  ], function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  });
}


exports.getSuperCategory = function (req, res) {
  SuperCategory.findOne({
    _id: req.params.supcatid
  }, function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  });
}

exports.findSuperCategoryAttribute = function (req, res) {
  SuperCategory.find({
    _id: req.params.supId
  }, function (err, supercategories) {
    if (err) {
      res.status(500).json(err);
    } else {
      if (supercategories[0]) {
        var superCatAttribute = supercategories[0].attribute.filter(el => el.fieldType === 'Dropdown' && el.fieldEnable === true);

        /* var subCatAttribute = .filter(el => el.fieldType === 'Dropdown') */
        res.status(200).json(superCatAttribute);
      } else {
        res.status(200).json(supercategories);
      }
    }
  });
}

exports.findSubCategoryAttribute = function (req, res) {
  SuperCategory.aggregate([{ $unwind: "$mainCategory" },
  { $unwind: "$mainCategory.subCategory" }, { $match: { 'mainCategory.subCategory._id': ObjectId(req.params.subId) } }]).exec(function (err, subcategories) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      if (subcategories[0]) {
        var subCatAttribute = subcategories[0].mainCategory.subCategory.attribute.filter(el => el.fieldType === 'Dropdown' && el.fieldEnable === true);
        res.status(200).json(subCatAttribute);
      } else {
        res.status(200).json(subcategories);
      }
    }
  });
}

exports.superCategorySingle = function (req, res) {
SuperCategory.findOne({
    _id: req.params.selectId
  }, function (err, supercategories) {
    if (err) {
      res.status(500).json(err);
    } else {
      if (supercategories) {
        var fixedValue = supercategories.attribute.sort(function (a, b) { return a.sortOrder - b.sortOrder });
        res.status(200).json(fixedValue);
      } else {
        SuperCategory.aggregate([{ $unwind: "$mainCategory"},
      { $match : {'mainCategory._id' : ObjectId(req.params.selectId)} }]).exec(function (err, maincategories) {
        if (err) {
          res.status(500).send({
            "result": 0
          });
        } else {
          if (maincategories[0]) {
            var subCatAttribute = maincategories[0].mainCategory.attribute;/* .filter(el => el.fieldType === 'Dropdown'); */
            res.status(200).json(subCatAttribute);
          } else { 
            SuperCategory.aggregate([{ $unwind: "$mainCategory" },
            { $unwind: "$mainCategory.subCategory" }, { $match: { 'mainCategory.subCategory._id': ObjectId(req.params.selectId) } }]).exec(function (err, subcategories) {
              if (err) {
                res.status(500).send({
                  "result": 0
                });
              } else {
                if (subcategories[0]) {
                  var subCatAttribute = subcategories[0].mainCategory.subCategory.attribute;/* .filter(el => el.fieldType === 'Dropdown'); */
                  res.status(200).json(subCatAttribute);
                } else {
                  res.status(200).json(subcategories);
                }
              }
    
            });
          }
        }
      });
      
      }
    }
  });
}