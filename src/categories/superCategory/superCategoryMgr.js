var superCategoryDA = require('../../categories/superCategory/superCategoryDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');


exports.superCategoryInsert = function (req, res) {
    try {
        superCategoryDA.superCategoryInsert(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.superCategoryShow = function (req, res) {
    try {
        superCategoryDA.superCategoryShow(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.superCategoryWithAttribute = function (req, res) {
    try {
        superCategoryDA.superCategoryWithAttribute(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.superCategorySingle = function (req, res) {
    try {
        superCategoryDA.superCategorySingle(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.superCategoryDelete = function (req, res) {
    try {
        superCategoryDA.superCategoryDelete(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.showSuperCategory = function (req, res) {
    try {
        superCategoryDA.showSuperCategory(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.superCategoryEdit = function (req, res) {
    try {
        superCategoryDA.superCategoryEdit(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.showSuperCategoryByStatus = function (req, res) {
    try {
        superCategoryDA.showSuperCategoryByStatus(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.supcat = function (req, res) {
    try {
        superCategoryDA.supcat(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getSuperCategory = function (req, res) {
    try {
        superCategoryDA.getSuperCategory(req, res);
    } catch (error) {
        console.log(error);
    }     
}
exports.findSuperCategoryAttribute = function (req, res) {
    try {
        superCategoryDA.findSuperCategoryAttribute(req, res);
    } catch (error) {
        console.log(error);
    }     
}
exports.findSubCategoryAttribute = function (req, res) {
    try {
        superCategoryDA.findSubCategoryAttribute(req, res);
    } catch (error) {
        console.log(error);
    }     
}