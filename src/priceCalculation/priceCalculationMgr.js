var PriceCalculationDA = require('./priceCalculationDA');

exports.addIncrementRate = function(req, res) {
    try { var currentDate = new Date();
        var date = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var toDay = month + '/' + date + '/' + year;
        PriceCalculationDA.addIncrementRate(req, res, toDay);
    } catch (error) {
        console.log(error);
    }
}

exports.getIncrementRate = function(req, res) {
    try {
        PriceCalculationDA.getIncrementRate(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.addPriceRate = function(req, res) {
    try { var currentDate = new Date();
        var date = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var toDay = month + '/' + date + '/' + year;
        PriceCalculationDA.addPriceRate(req, res, toDay);
    } catch (error) {
        console.log(error);
    }
}

exports.getPriceRate = function(req, res) {
    try {
        PriceCalculationDA.getPriceRate(req, res);
    } catch (error) {
        console.log(error);
    }
}