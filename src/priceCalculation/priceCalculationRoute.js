var PriceCalculationMgr = require('./priceCalculationMgr');

module.exports = function(app) {
    app.route('/addincrementrate')
        .post(PriceCalculationMgr.addIncrementRate);

    app.route('/getincrementrate')
        .get(PriceCalculationMgr.getIncrementRate);   

    app.route('/addpricerate')
        .post(PriceCalculationMgr.addPriceRate);

    app.route('/getpricerate')
        .get(PriceCalculationMgr.getPriceRate);   
}