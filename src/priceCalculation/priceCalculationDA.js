var IncrementRate = require('../model/incrementRate.model');
var PriceRate = require('../model/priceRate.model');

exports.addIncrementRate = function(req, res, date) {
    IncrementRate.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new IncrementRate(req.body);
                create.addedDate = date;
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                findData[0].incRate = req.body.incRate;
                findData[0].addedDate = date;
                findData[0].save(function(err, updateData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(updateData);
                    }
                })
            }
        }
    })
}
exports.getIncrementRate = function(req, res) {
    IncrementRate.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.addPriceRate = function(req, res, date) {
    PriceRate.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new PriceRate(req.body);
                create.addedDate = date;
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                findData[0].priceRate = req.body.priceRate;
                findData[0].addedDate = date;
                findData[0].save(function(err, updateData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(updateData);
                    }
                })
            }
        }
    })
}
exports.getPriceRate = function(req, res) {
    PriceRate.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}