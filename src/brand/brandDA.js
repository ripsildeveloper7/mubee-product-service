'use strict';
var Brand = require('./../model/brand.model');
var s3 = require('../config/s3.config');
var env = require('../config/s3.env');
const AWS = require('aws-sdk');
var PincodeDetails = require('../model/pincodeDetails.model');
var PincodeMapping = require('../model/pincodeMapping.model');

exports.addBrand = function (req, res) {
  var brand = new Brand();
  brand.brandName = req.body.brandName;
  brand.brandTitle = req.body.brandTitle;
  brand.brandDescription = req.body.brandDescription;
  brand.brandId = req.body.brandId;
  brand.brandStatus = req.body.brandStatus;
  brand.metaTitle = req.body.metaTitle;
  brand.metaDescription = req.body.metaDescription;
  brand.metaContent = req.body.metaContent;
  brand.save(function (err, brandData) {
    if (err) { // if it contains error return 0
      res.status(500).send({
        "result": 0
      });
    } else {
      res.status(200).json(brandData)
    }
  });
}


exports.getBrand = function (req, res) {
  Brand.find({}).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
});
}



exports.deleteBrand = function (req, res) {
  Brand.findOne({
    '_id': req.params.id
  }, function (err, brandDetails) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      s3.deleteObject({
        Bucket: env.Bucket,
        Key: 'images' + '/' + 'brand' + '/' + req.params.id + '/' + brandDetails.brandImageName
      }, function (err, bucketImage) {
        if (err) {
          res.status(500).json(err);
        } else {
          Brand.findByIdAndRemove(req.params.id, function (err, data) {
            if (err) {
              res.status(500).send({
                "result": 0
              });
            } else {
              Brand.find({}).select().exec(function (err, allbrand) {
                if (err) {
                  res.status(500).send({
                    message: "Some error occurred while retrieving notes."
                  });
                } else {
                  res.status(200).json(allbrand);
                }
              });
            }
          });
        }
      });
    }
  });
}

exports.findSingleBrand = function (req, res) {
  Brand.findOne({
    '_id': req.params.id
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  });
}


exports.updateBrand = function (req, res) {
  Brand.findOne({
    '_id': req.params.id
  }).select().exec(function (err, brand) {
    if (err) {
      res.status(500).json(err);
    } else {
      brand.brandName = req.body.brandName;
      brand.brandTitle = req.body.brandTitle;
      brand.brandDescription = req.body.brandDescription;
      brand.brandId = req.body.brandId;
      brand.brandStatus = req.body.brandStatus;
      brand.metaTitle = req.body.metaTitle;
      brand.metaDescription = req.body.metaDescription;
      brand.metaContent = req.body.metaContent;
      brand.save(function (err, data) {
        if (err) {
          res.status(500).json(err);
        } else {
          res.status(200).json(data);
        }
      });
    }
  });
}


exports.uploadBrandImagesName = function (req,  res) {
  Brand.findOne({
    '_id': req.params.id
  }, function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      data.brandImageName = req.body.brandImageName;
      data.save(function (err, brandImageSave) {
        if (err) {
          res.status(500).send({
            message: 1
          });
        } else {
          Brand.find({}).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      });
    }
  });
}

exports.uploadPincodeDetails = function (req, res) {
  var createB2cMarketData = [];
  var pincodeDetails = new PincodeMapping();
  for (var i = 0; i <= req.body.length-1; i++) {
      var b2cMarketCustomer = [];
      b2cMarketCustomer[i] = new PincodeDetails();
      b2cMarketCustomer[i].City = req.body[i].City;
      b2cMarketCustomer[i].Pincode = req.body[i].Pincode;
      b2cMarketCustomer[i].HasCod = req.body[i].HasCod;
      b2cMarketCustomer[i].HasPrepaid = req.body[i].HasPrepaid;
      b2cMarketCustomer[i].HasReverse = req.body[i].HasReverse;
      b2cMarketCustomer[i].ShiprocketDeliveryZone = req.body[i].ShiprocketDeliveryZone;
      b2cMarketCustomer[i].RoutingCode = req.body[i].RoutingCode;
      b2cMarketCustomer[i].State = req.body[i].State;
      b2cMarketCustomer[i].TAT = req.body[i].TAT;
      createB2cMarketData.push(b2cMarketCustomer[i]);
  }
  pincodeDetails.brandId = req.params.id;
  pincodeDetails.pincodeDetails = createB2cMarketData;
  pincodeDetails.save( function (err, sendData) {
      if (err) {
          console.log(err);
          res.status(500).send('Error Data');
      }
      res.status(200).json(sendData);
      console.log('send:', sendData);
  });
}

exports.getAllPincode = function (req, res) {
  PincodeMapping.find({}).select().exec(function (err, allPincode) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(allPincode);
    }
  });
}

exports.deletePincodeMapping = function (req, res) {
  PincodeMapping.findByIdAndRemove(req.params.id, function (err, data) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      PincodeMapping.find({}).select().exec(function (err, pincodemapping) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(pincodemapping);
        }
      });
    }
  });
}

exports.brandPincode = function (req, res) {
/*   PincodeMapping.find({
    brandId:req.params.id
  }).select().exec(function (err, allPincode) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(allPincode);
    }
  }); */
  PincodeMapping.findOne( {'brandId': req.params.id , 'pincodeDetails.Pincode': req.params.pincode }, {'pincodeDetails.$':1}).select().exec(function(err, data) {
    if(err) {
      console.log(err);
    } else {
      console.log(data);
      res.status(200).json(data)
    }
  })
}
