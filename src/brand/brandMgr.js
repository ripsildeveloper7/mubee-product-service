var brandDA = require('./brandDA');
var s3 = require('../config/s3.config');
var env = require('../config/s3.env');
const AWS = require('aws-sdk');

exports.addBrandDetails = function (req, res) {
  try {
    brandDA.addBrand(req, res);
  } catch (error) {
    console.log(error);
  }
}


exports.getBrand = function (req, res) {
  try {
    brandDA.getBrand(req, res);
  } catch (error) {
    console.log(error);
  }
}


exports.deleteBrand = function (req, res) {
  try {
    brandDA.deleteBrand(req, res);
  } catch (error) {
    console.log(error);
  }
}


exports.findSingleBrand = function (req, res) {
  try {
    brandDA.findSingleBrand(req, res);
  } catch (error) {
    console.log(error);
  }
}


exports.updateBrand = function (req, res) {
  try {
    brandDA.updateBrand(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.uploadBrandImagesName = function (req, res) {
    try {
      brandDA.uploadBrandImagesName(req, res);
    } catch (error) {
      console.log(error);
    }
  }

  exports.uploadPincodeDetails = function (req, res) {
    try {
      brandDA.uploadPincodeDetails(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  exports.getAllPincode = function (req, res) {
    try {
      brandDA.getAllPincode(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  exports.deletePincodeMapping = function (req, res) {
    try {
      brandDA.deletePincodeMapping(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  exports.brandPincode = function (req, res) {
    try {
      brandDA.brandPincode(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  
  