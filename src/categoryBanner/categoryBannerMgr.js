var CategoryBannerDA = require('./categoryBannerDA');

exports.createCategoryBanner = function(req, res) {
    try {
        CategoryBannerDA.createCategoryBanner(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.addImageName = function(req, res) {
    try {
        CategoryBannerDA.addImageName(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getCategoryBanner = function(req, res) {
    try {
        CategoryBannerDA.getCategoryBanner(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getSingleCategoryBanner = function(req, res) {
    try {
        CategoryBannerDA.getSingleCategoryBanner(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.updateCategoryBanner = function(req, res) {
    try {
        CategoryBannerDA.updateCategoryBanner(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.updateImageName = function(req, res) {
    try {
        CategoryBannerDA.updateImageName(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteCategoryBanner = function(req, res) {
    try {
        CategoryBannerDA.deleteCategoryBanner(req, res);
    } catch (error) {
        console.log(error);
    }
}