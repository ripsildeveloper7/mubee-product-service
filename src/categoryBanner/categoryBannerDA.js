var CategoryBanner = require('../model/categoryBanner.model');
var s3 = require('./../config/s3.config');
var env = require('./../config/s3.env');
const AWS = require('aws-sdk');

exports.createCategoryBanner = function (req, res) {
    var create = new CategoryBanner(req.body);
    create.save(function (err, saveData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(saveData);
        }
    })
}
exports.addImageName = function (req, res) {
    CategoryBanner.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.imageName = req.body.categoryBannerImage;
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}
exports.getCategoryBanner = function (req, res) {
    CategoryBanner.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.getSingleCategoryBanner = function (req, res) {
    CategoryBanner.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.updateCategoryBanner = function (req, res) {
    CategoryBanner.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.title = req.body.title;
            findData.description = req.body.description;
            findData.imagePosition = req.body.imagePosition;
            findData.link = req.body.link;
            findData.verticalPosition = req.body.verticalPosition;
            findData.horizontalPosition = req.body.horizontalPosition;
            findData.save(function (err, updateData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(updateData);
                }
            })
        }
    })
}
exports.updateImageName = function (req, res) {
    CategoryBanner.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.imageName === req.body.categoryBannerImage) {
                res.status(200).json(findData);
            } else {
                var s3 = new AWS.S3();
                s3.deleteObject({
                    Bucket: env.Bucket,
                    Key: 'images' + '/' + 'categorybanner' + '/' + req.params.id + '/' + findData.imageName
                }, function (err, data) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        findData.imageName = req.body.categoryBannerImage;
                        findData.save(function (err, updateData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(updateData);
                            }
                        })
                    }
                })
            }
        }
    })
}
exports.deleteCategoryBanner = function (req, res) {

    CategoryBanner.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var s3 = new AWS.S3();
            s3.deleteObject({
                Bucket: env.Bucket,
                Key: 'images' + '/' + 'categorybanner' + '/' + req.params.id + '/' + findData.imageName
            }, function (err, imageDelete) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    CategoryBanner.findOneAndRemove({
                        '_id': req.params.id
                    }).select().exec(function (err, deleteData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            CategoryBanner.find({}).select().exec(function (err, findData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    res.status(200).json(findData);
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}