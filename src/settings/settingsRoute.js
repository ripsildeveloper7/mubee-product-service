'use strict';
var settingsMgr = require('./settingsMgr');
var upload = require('../config/multer.config');
module.exports = function (app) {
    app.route('/productSettings')
        .get(settingsMgr.getProductSettings); // retreive all the details from product settings collection

    app.route('/pricerange')
        .post(settingsMgr.createPriceRange); // create a price range        

    app.route('/removeprice')
        .post(settingsMgr.deletePriceRange); //  delete single prince range

    app.route('/color')
        .post(settingsMgr.createColor); // create a color

    app.route('/removecolor')
        .post(settingsMgr.deleteColor); //remove single color
    app.route('/material')
        .post(settingsMgr.createMaterial); // create a material
    app.route('/removematerial')
        .post(settingsMgr.deleteMaterial); // remove single material 

        app.route('/note')
        .post(settingsMgr.createNote); // create a note
    app.route('/removenote')
        .post(settingsMgr.deleteNote); // remove single note 

    app.route('/occasion')
        .post(settingsMgr.createOccasion); // create an occasion

    app.route('/removeoccasion')
        .post(settingsMgr.deleteOccasion); // remove single occasion

    app.route('/size')
        .post(settingsMgr.createSize); // create an size

    app.route('/removesize')
        .post(settingsMgr.deleteSize); // remove single size

    app.route('/tags')
        .post(settingsMgr.createTags); // create an size

    app.route('/removetags')
        .post(settingsMgr.deleteTags); // remove single size



    // product option  start
    app.route('/createproductoption')
        .post(settingsMgr.createProductOption); // create product options
    app.route('/getproductoption')
        .get(settingsMgr.getProductOption); // get product options
    app.route('/editproductoption/:id')
        .put(settingsMgr.editProductOption); // edit product options
    app.route('/deleteproductoption/:id')
        .delete(settingsMgr.deleteProductOption); // delete product options
    app.route('/getsingleproductoption/:id')
        .get(settingsMgr.getSingleProductOption); // delete product options


    /* app.route('/editproductoption/:id/optionvalue/:optionId')
    .put(settingsMgr.editProductOptionValue);   */
    // product option  end

    // product tag start //
    app.route('/addproducttag') // add product tag
        .post(settingsMgr.addProductTag);
    app.route('/getproducttag') // get product tag
        .get(settingsMgr.getProductTag);
    app.route('/deleteproducttag/:id') // delete product tag
        .delete(settingsMgr.deleteProductTag);
    app.route('/getselecetedproducttag/:id') // get Selected tag
        .get(settingsMgr.getSelectedProductTag);

    app.route('/editproducttag/:id')
        .put(settingsMgr.editProductTag); // edit product tag

    app.route('/getproducttagvalue') // get product tag value
        .get(settingsMgr.findtag);
    app.route('/getfirsttag') // get product tag value
        .get(settingsMgr.firstTag);

    app.route('/getsecondtag') // get product tag value
        .get(settingsMgr.secondTag);

    app.route('/getproductposition/:id') // get product position
        .get(settingsMgr.getProductPosition);

    // add size guide
    app.route('/sizechartcmname/:id')
        .put(settingsMgr.addSizeCharCMName);
        app.route('/sizechartinchesname/:id')
        .put(settingsMgr.addSizeCharInchesName);

    app.route('/addsizeguide')
        .post(settingsMgr.addSizeGuide);
    app.route('/sizeguide')
        .get(settingsMgr.getAllSizeGuide);
    app.route('/deletesizechartcm/:id')                     // delete Size Chart CM
        .delete(settingsMgr.deleteSizeCharCM);
        app.route('/deletesizechartinches/:id')             // delete Size Chart Inches
        .delete(settingsMgr.deleteSizeCharInches);
        app.route('/deletesizeguide/:id')
        .delete(settingsMgr.deleteSizeGuide);
    app.route('/getproductsizeguide/:id')
        .get(settingsMgr.getProductSizeGuide);
        app.route('/getsizeguidebysupercategory/:id/:bid')
        .get(settingsMgr.getSizeGuideBySuperCategory);
        app.route('/getsizeguidebymaincategory/:id/:bid')
        .get(settingsMgr.getSizeGuideByMainCategory);
        app.route('/getsizeguidebysubcategory/:id/:bid')
        .get(settingsMgr.getSizeGuideBySubCategory);


    app.route('/singlesizeguide/:id')
        .get(settingsMgr.getSingleSizeGuide);    
        app.route('/getcategoryforproduct/:superId/gettagforproduct/:tagId') // get product by tag and super category id
        .get(settingsMgr.getCategoryTagProduct);    

    app.route('/updateproductsizeguide/:id')
        .put(settingsMgr.updateProductSizeGuide);   

        app.route('/getsizeguide/:id')
        .get(settingsMgr.getSizeGuide);

        app.route('/adddiscountforsetting')
        .post(settingsMgr.createDiscount);     
        app.route('/deletediscountinsetting')
        .post(settingsMgr.deleteDiscount);     

        app.route('/adddispatchtimeforsetting')
        .post(settingsMgr.createDispatchTime);     
        app.route('/deletedispatchtimeinsetting')
        .post(settingsMgr.deleteDispatchTime);    
        
        app.route('/addhowtomeasure')
        .post(settingsMgr.addHowToMeasure);

        app.route('/deletehowtomeasure/:id')
        .delete(settingsMgr.deleteHowToMeasure);

        app.route('/gethowtomeasure')
        .get(settingsMgr.getHowToMeasure);

        app.route('/howtomeasurename/:id')
        .put(settingsMgr.addImageName);

        // app.route('/addhowtomeasureimage')
        // .put(settingsMgr.addHowToMeasureName);
}
