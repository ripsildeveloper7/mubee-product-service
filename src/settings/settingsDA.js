var ProductSettings = require('../model/productSettings.model');
var ProductOption = require('../model/productOption.model');
var ProductTags = require('../model/productTag.model');
var product = require('../model/product.model');
var SizeGuide = require('../model/sizeguide.model');
var HowToMeasure = require('../model/howtomeasure.model');
var s3 = require('../config/s3.config');
var env = require('../config/s3.env');
const AWS = require('aws-sdk');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

// product option  start


exports.createProductOption = function (req, res) {
  var productOption = new ProductOption();
  productOption.optionName = req.body.optionName;
  productOption.optionValue = req.body.optionValue;
  productOption.save(function (err, optionData) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(optionData);
    }
  });
}


exports.getProductOption = function (req, res) {
  ProductOption.find({}).select().exec(function (err, productoptions) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(productoptions);
    }
  });
}

exports.getSingleProductOption = function (req, res) {
  ProductOption.findOne({
    _id: req.params.id
  }).select().exec(function (err, productoption) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(productoption);
    }
  });
}

exports.editProductOption = function (req, res) {
  ProductOption.findOne({
    _id: req.params.id
  }).select().exec(function (err, productOption) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      productOption.optionName = req.body.optionName;
      productOption.optionValue = req.body.optionValue;
      productOption.save(function (err, optiondata) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(optiondata);
        }
      });
      /* res.status(200).json(productoption); */
    }
  });
}

/* 
exports.editProductOptionValue = function (req, res) {
    ProductOption.findOne({_id: req.params.id}).select().exec(function (err, productOption) {
      if (err) {
        res.status(500).send({
          message: "Some error occurred while retrieving notes."
        });
      } else {
        var optionValue = productOption.optionValue.id(req.params.optionId);
        optionValue.optionValueName = req.body.optionValueName;
        optionValue.sortOrder = req.body.sortOrder;
        productOption.save(function (err, optiondata) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(optiondata);
            }
          });
      }
    });
  } */

exports.deleteProductOption = function (req, res) {
  ProductOption.findByIdAndRemove(req.params.id, function (err) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      ProductOption.find({}).select().exec(function (err, allProductOptions) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(allProductOptions);
        }
      });
    }
  });
}

// product option end

exports.createPriceRange = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetailsData) {
    if (err) {
      res.status(500).send({
        "result": 'error occured while retreiving data'
      })
    } else {
      var details = new ProductSettings();
      details.priceRange.push(req.body.priceRange);
      if (productDetailsData.length == 0) {

        details.save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productDetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productDetails);
              }
            });
          }
        })


      } else {
        productDetailsData[0].priceRange.push(req.body.priceRange);
        productDetailsData[0].save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productdetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productdetails);
              }
            });
          }
        })

      }
    }
  });
}

exports.createPriceRange = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetailsData) {
    if (err) {
      res.status(500).send({
        "result": 'error occured while retreiving data'
      })
    } else {
      var details = new ProductSettings();
      details.priceRange.push(req.body.priceRange);
      if (productDetailsData.length == 0) {

        details.save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productDetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productDetails);
              }
            });
          }
        })


      } else {
        productDetailsData[0].priceRange.push(req.body.priceRange);
        productDetailsData[0].save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productdetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productdetails);
              }
            });
          }
        })

      }
    }
  });
}
exports.deletePriceRange = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetails) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var priceRangeVal = productDetails[0].priceRange;
      var index = priceRangeVal.indexOf(req.body.priceRange);
      priceRangeVal.splice(index, 1);
      productDetails[0].save(function (err, data) {
        if (err) {
          res.status(500).send({
            "result": 'error occured while saving data'
          })
        } else {
          ProductSettings.find({}).select().exec(function (err, productDetails) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(productDetails);
            }
          });
        }
      })
    }
  });
}

exports.createColor = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetailsData) {
    if (err) {
      res.status(500).send({
        "result": 'error occured while retreiving data'
      })
    } else {
      var details = new ProductSettings();
      details.color.push(req.body.color);
      if (productDetailsData.length == 0) {
        details.save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productDetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productDetails);
              }
            });
          }
        })
      } else {
        productDetailsData[0].color.push(req.body.color);
        productDetailsData[0].save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productdetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productdetails);
              }
            });
          }
        })

      }
    }
  });
}
exports.deleteColor = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetails) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var colorVal = productDetails[0].color;
      var index = colorVal.indexOf(req.body.color);
      colorVal.splice(index, 1);
      productDetails[0].save(function (err, data) {
        if (err) {
          res.status(500).send({
            "result": 'error occured while saving data'
          })
        } else {
          ProductSettings.find({}).select().exec(function (err, productDetails) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(productDetails);
            }
          });
        }
      })
    }
  });
}
exports.createMaterial = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetailsData) {
    if (err) {
      res.status(500).send({
        "result": 'error occured while retreiving data'
      })
    } else {
      var details = new ProductSettings();
      details.color.push(req.body.material);
      if (productDetailsData.length == 0) {

        details.save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productDetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productDetails);
              }
            });
          }
        })


      } else {
        productDetailsData[0].material.push(req.body.material);
        productDetailsData[0].save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productdetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productdetails);
              }
            });
          }
        })

      }
    }
  });
}


exports.createNote = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetailsData) {
    if (err) {
      res.status(500).send({
        "result": 'error occured while retreiving data'
      })
    } else {
      var details = new ProductSettings();
      details.note.push(req.body.note);
      if (productDetailsData.length === 0 ) {

        details.save(function (err, data) {
          if (err) {
            console.log(err);
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            console.log(data);
            ProductSettings.find({}).select().exec(function (err, productDetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productDetails);
              }
            });
          }
        })


      } else {
        productDetailsData[0].note.push(req.body.note);
        productDetailsData[0].save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productdetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productdetails);
              }
            });
          }
        })

      }
    }
  });
}

exports.deleteNote = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetails) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var noteVal = productDetails[0].note;
      var index = noteVal.indexOf(req.body.note);
      noteVal.splice(index, 1);
      productDetails[0].save(function (err, data) {
        if (err) {
          res.status(500).send({
            "result": 'error occured while saving data'
          })
        } else {
          ProductSettings.find({}).select().exec(function (err, productDetails) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(productDetails);
            }
          });
        }
      })
    }
  });
}


exports.getProductSettings = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productdetails) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(productdetails);
    }
  });
}
exports.deleteMaterial = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetails) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var materialVal = productDetails[0].material;
      var index = materialVal.indexOf(req.body.material);
      materialVal.splice(index, 1);
      productDetails[0].save(function (err, data) {
        if (err) {
          res.status(500).send({
            "result": 'error occured while saving data'
          })
        } else {
          ProductSettings.find({}).select().exec(function (err, productDetails) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(productDetails);
            }
          });
        }
      })
    }
  });
}

exports.createOccasion = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetailsData) {
    if (err) {
      res.status(500).send({
        "result": 'error occured while retreiving data'
      })
    } else {
      var details = new ProductSettings();
      details.occasion.push(req.body.occasion);
      if (productDetailsData.length == 0) {

        details.save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productDetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productDetails);
              }
            });
          }
        })


      } else {
        productDetailsData[0].occasion.push(req.body.occasion);
        productDetailsData[0].save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productdetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productdetails);
              }
            });
          }
        })

      }
    }
  });
}
exports.deleteOccasion = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetails) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var occasionVal = productDetails[0].occasion;
      var index = occasionVal.indexOf(req.body.occasion);
      occasionVal.splice(index, 1);
      productDetails[0].save(function (err, data) {
        if (err) {
          res.status(500).send({
            "result": 'error occured while saving data'
          })
        } else {
          ProductSettings.find({}).select().exec(function (err, productDetails) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(productDetails);
            }
          });
        }
      })
    }
  });
}

exports.createSize = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetailsData) {
    if (err) {
      res.status(500).send({
        "result": 'error occured while retreiving data'
      })
    } else {
      var details = new ProductSettings();
      details.size.push(req.body.size);
      if (productDetailsData.length == 0) {

        details.save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productDetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productDetails);
              }
            });
          }
        })


      } else {
        productDetailsData[0].size.push(req.body.size);
        productDetailsData[0].save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productdetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productdetails);
              }
            });
          }
        })

      }
    }
  });
}
exports.deleteSize = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetails) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var sizeVal = productDetails[0].size;
      var index = sizeVal.indexOf(req.body.size);
      sizeVal.splice(index, 1);
      productDetails[0].save(function (err, data) {
        if (err) {
          res.status(500).send({
            "result": 'error occured while saving data'
          })
        } else {
          ProductSettings.find({}).select().exec(function (err, productDetails) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(productDetails);
            }
          });
        }
      })
    }
  });
}
exports.createTags = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetailsData) {
    if (err) {
      res.status(500).send({
        "result": 'error occured while retreiving data'
      })
    } else {
      var details = new ProductSettings();
      details.tags.push(req.body.tags);
      if (productDetailsData.length == 0) {
        details.save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productDetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productDetails);
              }
            });
          }
        })
      } else {
        productDetailsData[0].tags.push(req.body.tags);
        productDetailsData[0].save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productdetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productdetails);
              }
            });
          }
        })

      }
    }
  });
}
exports.deleteTags = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetails) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var tagsVal = productDetails[0].tags;
      var index = tagsVal.indexOf(req.body.tags);
      tagsVal.splice(index, 1);
      productDetails[0].save(function (err, data) {
        if (err) {
          res.status(500).send({
            "result": 'error occured while saving data'
          })
        } else {
          ProductSettings.find({}).select().exec(function (err, productDetails) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(productDetails);
            }
          });
        }
      })
    }
  });
}

exports.addProductTag = function (req, res) {
  var create = new ProductTags();
  create.tagName = req.body.tagName;
  create.position = req.body.position;
  create.tagValue = req.body.tagValue;
  create.save(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}

exports.getProductTag = function (req, res) {
  ProductTags.find({}).sort({
    'position': 1
  }).exec(function (err, finData) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(finData);
    }
  })
}
exports.deleteProductTag = function (req, res) {
  ProductTags.findByIdAndRemove(req.params.id, function (err) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      ProductTags.find({}).select().exec(function (err, allProductTag) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(allProductTag);
        }
      });
    }
  });
}

exports.getSelectedProductTag = function (req, res) {
  ProductTags.findOne({
    '_id': req.params.id
  }).select().exec(function (err, finData) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(finData);
    }
  })
}

exports.editProductTag = function (req, res) {
  ProductTags.findOne({
    _id: req.params.id
  }).select().exec(function (err, productTag) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      productTag.tagName = req.body.tagName;
      productTag.position = req.body.position;
      productTag.tagValue = req.body.tagValue;
      productTag.save(function (err, tagdata) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(tagdata);
        }
      });

    }
  });
}
exports.findtag = function (req, res) {
  product.aggregate([{
    $lookup: {
      from: 'products',
      localField: '_id',
      foreignField: 'tags',
      as: 'productTag'
    }
  }], function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}
exports.firstTag = function (req, res) {
  ProductTags.find({
    'position': 1
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}

exports.secondTag = function (req, res) {
  ProductTags.find({
    'position': 2
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}
// Size Guide  ----------------------------- Start ------------------------------------------------

exports.addSizeGuide = function (req, res) {
  var create = new SizeGuide();
  create.sizeGuideName = req.body.sizeGuideName;
  create.superCategoryId = req.body.superCategoryId;
  create.mainCategoryId = req.body.mainCategoryId;
  create.subCategoryId = req.body.subCategoryId;
  create.sizeChartCM = req.body.sizeChartCM;
  create.sizeChartInches = req.body.sizeChartInches;
  create.superCategoryName = req.body.superCategoryName;
  create.mainCategoryName = req.body.mainCategoryName;
  create.subCategoryName = req.body.subCategoryName;
  create.brandId = req.body.brandId;
  create.brandName = req.body.brandName;
  create.save(function (err, sizeguidedata) {
    if (err) { // if it contains error return 0
      res.status(500).send({
        "result": 'error occured while retreiving data'
      });
    } else {
      res.status(200).json(sizeguidedata);
    }
  });
}
exports.getAllSizeGuide = function (req, res) {                // find all size guide
  SizeGuide.find({}).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}

exports.deleteSizeGuide = function (req, res) {
  SizeGuide.findOne({_id: req.params.id}).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
     /*  var s3 = new AWS.S3(); */
      if (data.sizeChartCM !== undefined) {
        s3.deleteObject({
          Bucket: env.Bucket,
          Key: 'images' + '/' + 'size' + '/' + req.params.id + '/' + data.sizeChartCM
        }, function (err, data) {
          if (err) {
            res.status(500).json(err);
          } else {
            SizeGuide.findOneAndRemove({'_id': req.params.id}).select().exec(function(err, deleteData) {
              if (err) {
                res.status(500).json(err);
              } else {
                res.status(200).json(deleteData);
              }
            })
          }
        });
      } else {
        SizeGuide.findOneAndRemove({'_id': req.params.id}).select().exec(function(err, deleteData) {
          if (err) {
            res.status(500).json(err);
          } else {
            res.status(200).json(deleteData);
          }
        })
      }
     
    }
  });
}




exports.deleteSizeCharCM = function (req, res) {
  SizeGuide.findByIdAndRemove(req.params.id, function (err, data) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var s3 = new AWS.S3();
      s3.deleteObject({
        Bucket: env.Bucket,
        Key: 'images' + '/' + 'size' + '/' + req.params.id + '/' + req.body.sizeChartCM
      }, function (err, data) {
        if (err) {
          res.status(500).json(err);
        } else {
           SizeGuide.find({}).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      });
    }
  });
}
exports.deleteSizeCharInches = function (req, res) {
  SizeGuide.findByIdAndRemove(req.params.id, function (err, data) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var s3 = new AWS.S3();
      s3.deleteObject({
        Bucket: env.Bucket,
        Key: 'images' + '/' + 'size' + '/' + req.params.id + '/' + req.body.SizeChartInches
      }, function (err, data) {
        if (err) {
          res.status(500).json(err);
        } else {
           SizeGuide.find({}).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      });
    }
  });
}


exports.getProductPosition = function(req, res) {
  product.find({'tags': ObjectId(req.params.id)}).select().exec(function(err, data) {
    if(err) {
      res.status(500).json(err);
    } else {
     /*  for (let i = 0; i <= data.length - 1; i++) {
        var firstValue = data[i].productImageName.length - 1;
        for (let j = 0; j <= firstValue; j++) {
          data[i].productImageName[j] = env.ImageServerPath + 'product' + '/' + data[i]._id + '/' + data[i].productImageName[j];
        }
      } */
      res.status(200).json(data);
    }
  })
}
exports.addSizeCharCMName = function (req, res) {
  SizeGuide.findOne({
    '_id': req.params.id
  }, function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      data.sizeChartCM = req.body.sizeChartCM;
      data.save(function (err, sizeguide) {
        if (err) {
          console.log(err);
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(sizeguide);
        }
      });
    }
  });
}
exports.addSizeCharInchesName = function (req, res) {
  SizeGuide.findOne({
    '_id': req.params.id
  }, function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      data.imageName = req.body.imageName;
      data.sizeChartInches = req.body.sizeChartInches;
      data.save(function (err, sizeguide) {
        if (err) {
          console.log(err);
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(sizeguide);
        }
      });
    }
  });
}
exports.getProductSizeGuide = function(req, res) {
  SizeGuide.findOne({
    '_id': req.params.id
  }).select().exec(function(err, data) {
    if(err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}
exports.getSizeGuideBySuperCategory = function(req, res) {
  SizeGuide.find({
    'superCategoryId': req.params.id,
    'brandId': req.params.bid
  }).select().exec(function(err, data) {
    if(err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}

exports.getSizeGuideByMainCategory = function(req, res) {
  SizeGuide.find({
    'mainCategoryId': req.params.id , 'brandId': req.params.bid
  }).select().exec(function(err, data) {
    if(err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}

exports.getSizeGuideBySubCategory = function(req, res) {
  SizeGuide.find({
    'subCategoryId': req.params.id,
    'brandId': req.params.bid
  }).select().exec(function(err, data) {
    if(err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}

exports.getSizeGuide = function(req, res){
  SizeGuide.findOne({
    '_id' : req.params.id 
  }).select().exec(function(err, data){
    if(err){
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}
exports.getCategoryTagProduct = function(req, res) {
  product.find({ 'superCategoryId': req.params.superId,
                 'tags': req.params.tagId
  }).select().exec(function(err, data) {
    if(err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}
exports.getSingleSizeGuide = function (req, res) {  // findOne
  SizeGuide.findOne({'_id': req.params.id}).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}



exports.updateProductSizeGuide = function(req, res) {
  SizeGuide.findOne({'_id': req.params.id}).select().exec(function(err, sizeGuide) {
    if (err) {
      res.status(500).json(err);
    } else {
      sizeGuide.title = req.body.title;
      sizeGuide.save(function(err, saveData) {
        if (err) {
          res.status(500).json(err);
        } else {
          res.status(200).json(saveData);
        }
      })
    }
  })
}



exports.createDiscount = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetailsData) {
    if (err) {
      res.status(500).send({
        "result": 'error occured while retreiving data'
      })
    } else {
      var details = new ProductSettings();
      details.discount.push(req.body.discount);
      if (productDetailsData.length == 0) {
        details.save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productDetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productDetails);
              }
            });
          }
        })
      } else {
        productDetailsData[0].discount.push(req.body.discount);
        productDetailsData[0].save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productdetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productdetails);
              }
            });
          }
        })
      }
    }
  });
}
exports.deleteDiscount = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetails) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var discountVal = productDetails[0].discount;
      var index = discountVal.indexOf(req.body.discount);
      discountVal.splice(index, 1);
      productDetails[0].save(function (err, data) {
        if (err) {
          res.status(500).send({
            "result": 'error occured while saving data'
          })
        } else {
          ProductSettings.find({}).select().exec(function (err, productDetails) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(productDetails);
            }
          });
        }
      })
    }
  });
}
exports.createDispatchTime = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetailsData) {
    if (err) {
      res.status(500).send({
        "result": 'error occured while retreiving data'
      })
    } else {
      var details = new ProductSettings();
      details.dispatchTime.push(req.body.dispatchTime);
      if (productDetailsData.length == 0) {
        details.save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productDetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productDetails);
              }
            });
          }
        })
      } else {
        productDetailsData[0].dispatchTime.push(req.body.dispatchTime);
        productDetailsData[0].save(function (err, data) {
          if (err) {
            res.status(500).send({
              "result": 'error occured while saving data'
            })
          } else {
            ProductSettings.find({}).select().exec(function (err, productdetails) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(productdetails);
              }
            });
          }
        })
      }
    }
  });
}
exports.deleteDispatchTime = function (req, res) {
  ProductSettings.find({}).select().exec(function (err, productDetails) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var dispatchTimeVal = productDetails[0].dispatchTime;
      var index = dispatchTimeVal.indexOf(req.body.dispatchTime);
      dispatchTimeVal.splice(index, 1);
      productDetails[0].save(function (err, data) {
        if (err) {
          res.status(500).send({
            "result": 'error occured while saving data'
          })
        } else {
          ProductSettings.find({}).select().exec(function (err, productDetails) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(productDetails);
            }
          });
        }
      })
    }
  });
}


exports.addHowToMeasure = function (req, res) {
  var create = new HowToMeasure();
  create.superCategoryId = req.body.superCategoryId;
  create.mainCategoryId = req.body.mainCategoryId;
  create.subCategoryId = req.body.subCategoryId;
  create.imageName = req.body.imageName;
  create.superCategoryName = req.body.superCategoryName;
  create.mainCategoryName = req.body.mainCategoryName;
  create.subCategoryName = req.body.subCategoryName;
  create.name = req.body.name;
  create.detail = req.body.detail;
  create.save(function (err, howtomeasuredata) {
    if (err) { // if it contains error return 0
      res.status(500).send({
        "result": 'error occured while retreiving data'
      });
    } else {
      res.status(200).json(howtomeasuredata);
    }
  });
}

exports.getHowToMeasure = function (req, res) {                // find all size guide
  HowToMeasure.find({}).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}
exports.deleteHowToMeasure = function (req, res) {
  HowToMeasure.findByIdAndRemove(req.params.id, function (err, data) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var s3 = new AWS.S3();
      s3.deleteObject({
        Bucket: env.Bucket,
        Key: 'images' + '/' + 'howtomeasure' + '/' + req.params.id + '/' + req.body.imageName
      }, function (err, data) {
        if (err) {
          res.status(500).json(err);
        } else {
          HowToMeasure.find({}).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      });
    }
  });
}

exports.addImageName = function (req, res) {
  HowToMeasure.findOne({
    '_id': req.params.id
  }, function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      data.imageName = req.body.image;
      data.save(function (err, howtomeasure) {
        if (err) {
          console.log(err);
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(howtomeasure);
        }
      });
    }
  });
}

// exports.addHowToMeasureName = function (req, res) {
//   HowToMeasure.findOne({
//     '_id': req.params.id
//   }, function (err, data) {
//     if (err) {
//       res.status(500).send({
//         message: "Some error occurred while retrieving notes."
//       });
//     } else {
//       data.howtomeasureimg = req.body.imageName;
//       data.save(function (err, howtomeasure) {
//         if (err) {
//           console.log(err);
//           res.status(500).send({
//             message: "Some error occurred while retrieving notes."
//           });
//         } else {
//           res.status(200).json(h);
//         }
//       });
//     }
//   });
// }