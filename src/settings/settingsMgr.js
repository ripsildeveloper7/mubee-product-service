var settingsDA = require('./settingsDA');
var s3 = require('../config/s3.config');
var env = require('../config/s3.env');

// product option  start

exports.createProductOption = function (req, res) {
    try {
        settingsDA.createProductOption(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getProductOption = function (req, res) {
    try {
        settingsDA.getProductOption(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getSingleProductOption = function (req, res) {
    try {
        settingsDA.getSingleProductOption(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.editProductOption = function (req, res) {
    try {
        settingsDA.editProductOption(req, res);
    } catch (error) {
        console.log(error);
    }
}



exports.deleteProductOption = function (req, res) {
    try {
        settingsDA.deleteProductOption(req, res);
    } catch (error) {
        console.log(error);
    }
}

/* 
exports.editProductOptionValue = function (req, res) {
    try {
        settingsDA.editProductOptionValue(req, res);
    } catch (error) {
        console.log(error);
    }
} */

// product option  end

exports.getProductSettings = function (req, res) {
    try {
        settingsDA.getProductSettings(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.createPriceRange = function (req, res) {
    try {
        settingsDA.createPriceRange(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deletePriceRange = function (req, res) {
    try {
        settingsDA.deletePriceRange(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.createColor = function (req, res) {
    try {
        settingsDA.createColor(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteColor = function (req, res) {
    try {
        settingsDA.deleteColor(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.createNote = function (req, res) {
    try {
        settingsDA.createNote(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteNote = function (req, res) {
    try {
        settingsDA.deleteNote(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.createMaterial = function (req, res) {
    try {
        settingsDA.createMaterial(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteMaterial = function (req, res) {
    try {
        settingsDA.deleteMaterial(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.createOccasion = function (req, res) {
    try {
        settingsDA.createOccasion(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteOccasion = function (req, res) {
    try {
        settingsDA.deleteOccasion(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.createSize = function (req, res) {
    try {
        settingsDA.createSize(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteSize = function (req, res) {
    try {
        settingsDA.deleteSize(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.createTags = function (req, res) {
    try {
        settingsDA.createTags(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteTags = function (req, res) {
    try {
        settingsDA.deleteTags(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.addProductTag = function (req, res) {
    try {
        settingsDA.addProductTag(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getProductTag = function (req, res) {
    try {
        settingsDA.getProductTag(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteProductTag = function (req, res) {
    try {
        settingsDA.deleteProductTag(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getSelectedProductTag = function (req, res) {
    try {
        settingsDA.getSelectedProductTag(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.editProductTag = function (req, res) {
    try {
        settingsDA.editProductTag(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.findtag = function (req, res) {
    try {
        settingsDA.findtag(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.firstTag = function (req, res) {
    try {
        settingsDA.firstTag(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.secondTag = function (req, res) {
    try {
        settingsDA.secondTag(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getProductPosition = function (req, res) {
    try {
        settingsDA.getProductPosition(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.addSizeGuide = function (req, res) {
    try {
        settingsDA.addSizeGuide(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getSizeGuide = function (req, res) {
    try {
        settingsDA.getSizeGuide(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getSuperCategorySizeGuide = function (req, res) {
    try{
        settingsDA.getSuperCategorySizeGuide(req,res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteSizeCharCM = function (req, res) {
    try {
        settingsDA.deleteSizeCharCM(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteSizeCharInches = function (req, res) {
    try {
        settingsDA.deleteSizeCharInches(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteSizeGuide = function (req, res) {
    try {
        settingsDA.deleteSizeGuide(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.addSizeCharCMName = function (req, res) {
    try {
        settingsDA.addSizeCharCMName(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.addSizeCharInchesName = function (req, res) {
    try {
        settingsDA.addSizeCharInchesName(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getProductSizeGuide = function (req, res) {
    try {
        settingsDA.getProductSizeGuide(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getSizeGuideBySuperCategory = function (req, res) {
    try {
        settingsDA.getSizeGuideBySuperCategory(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getSizeGuideByMainCategory = function (req, res) {
    try {
        settingsDA.getSizeGuideByMainCategory(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getSizeGuideBySubCategory = function (req, res) {
    try {
        settingsDA.getSizeGuideBySubCategory(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getAllSizeGuide = function(req,res) {
    try {
        settingsDA.getAllSizeGuide(req,res);
    } catch(error) {
        console.log(error);
    }
}
exports.getCategoryTagProduct = function (req, res) {
    try {
        settingsDA.getCategoryTagProduct(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getSingleSizeGuide = function (req, res) {
    try {
        settingsDA.getSingleSizeGuide(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.updateProductSizeGuide = function (req, res) {
    try {
        settingsDA.updateProductSizeGuide(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.createDiscount = function (req, res) {
    try {
        settingsDA.createDiscount(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteDiscount = function (req, res) {
    try {
        settingsDA.deleteDiscount(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.createDispatchTime = function (req, res) {
    try {
        settingsDA.createDispatchTime(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteDispatchTime = function (req, res) {
    try {
        settingsDA.deleteDispatchTime(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.addHowToMeasure = function (req, res) {
    try {
        settingsDA.addHowToMeasure(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getHowToMeasure = function (req, res) {
    try {
        settingsDA.getHowToMeasure(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteHowToMeasure = function (req, res) {
    try {
        settingsDA.deleteHowToMeasure(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.addImageName = function (req, res) {
    try {
        settingsDA.addImageName(req, res);
    } catch (error) {
        console.log(error);
    }
}

// exports.addHowToMeasureName = function (req, res) {
//     try {
//         settingsDA.addHowToMeasureName(req, res);
//     } catch (error) {
//         console.log(error);
//     }
// }